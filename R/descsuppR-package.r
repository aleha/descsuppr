#' descsuppR
#'
#' @name descsuppR
#' @docType package
#' @import foreach
#' @import nparcomp
#' @import rankFD
#' @importFrom descutils prettyPvalues
#' @importFrom stats anova
#' @importFrom stats chisq.test
#' @importFrom stats cor.test
#' @importFrom stats fisher.test
#' @importFrom stats kruskal.test
#' @importFrom stats lm
#' @importFrom stats median
#' @importFrom stats na.omit
#' @importFrom stats p.adjust
#' @importFrom stats sd
#' @importFrom stats t.test
#' @importFrom stats wilcox.test
#' @importFrom stats prop.trend.test
#' @importFrom stats quantile
#' @importFrom stats setNames
#' @importFrom DescTools JonckheereTerpstraTest
#' @importFrom DescTools CochranArmitageTest
#' @importFrom dplyr rename
#' @importFrom dplyr "%>%"
#' @importFrom dplyr full_join
#' @importFrom dplyr if_else
#' @importFrom dplyr mutate
#' @importFrom dplyr mutate_at
#' @importFrom dplyr select_if
#' @importFrom tibble as_tibble
#' @importFrom plyr failwith
#' @importFrom plyr join
#' @importFrom plyr ddply
#' @importFrom rlang .data
#' @importFrom circular circular
#' @importFrom glue glue
#' @importFrom purrr possibly
#'
#'
#' @keywords internal
"_PACKAGE"


##' @export
median.ordered <- function(x, na.rm=FALSE, ...)
{
  ## from https://stackoverflow.com/a/7925162

  if (na.rm) {
    x <- na.omit(x)
  }
  levs <- levels(x)
  m <- median(as.integer(x))
  if(floor(m) != m)
  {
    warning("Median is between two values; using the first one")
    m <- floor(m)
  }
  ordered(m, labels = levs, levels = seq_along(levs))
}

##' generate [lower quartile; upper quartile]
##'
##' wrapper around \code{quantile()} and \code{format()} and \code{paste()}.
##' @param x vector to be summarized
##' @param ... passed onto quantile() as well as to format().  meant for arguments \code{na.rm} or \code{digits}
##' @return character.
##' @author Dr. Andreas Leha
formatIQR <- function(x, digits = 2, significant_digits = TRUE, ...)
{ 
  if(is.ordered(x))
    paste0("[",
           paste(
             quantile(x, probs = c(0.25, 0.75), type = 1, ...),
             collapse = "; "),
           "]")
  else
    paste0("[",
           paste(
             formatC_mod(quantile(x, probs = c(0.25, 0.75), ...), format = ifelse(significant_digits, 'fg', 'f') ,flag = "#", digits = digits),
             collapse = "; "),
           "]")
}

##' Calculate the Time Difference in Years
##'
##' Code from Dirk Eddelbuettel via Stackoverflow
##' (\url{http://stackoverflow.com/a/15569373/1844418})
##' @param t2 end time of the interval.  Will be coerced to Date
##' @param t1 starting time of the interval.  Will be coerced to Date
##' @return numeric.  t2 - t1 [years]
##' @author Andreas Leha
##' @export
##' @examples
##' difftime_years("2003-04-05", "2001-01-01")
difftime_years <- function(t2, t1)
{
  t1 <- as.Date(t1)
  t2 <- as.Date(t2)
  td <- difftime(t2, t1, units="weeks")
  td <- as.numeric(td)
  ty <- td/52.25
  return(ty)
}


##' wrap all elements in a vector in quotes (or other strings)
##'
##' @param v vector of elements to wrap
##' @param quoteChr character.  to be put around of the elements of
##'   \code{v}.  Defaults to "'".
##' @param endquoteChr character or NULL (default).  If not NULL
##'   \code{quoteChr} is put before the elements of \code{v} and
##'   \code{endquoteChr} is put after them
##' @return character vector of the elements of \code{v} wrapped
##'   between quotes
##' @author Andreas Leha
##' @export
##' @examples
##' ## default behaviour: wrap in single quotes
##' wrapQuote(1:10)
##'
##' ## change to wrap in asterisks
##' wrapQuote(1:10, "*")
##'
##' ## different front from back quotes
##' wrapQuote(1:10, "*", "/")
##'
##' ## you can also wrap with longer strings
##' wrapQuote(1:10, "quote")
wrapQuote <- function(v, quoteChr = "'", endquoteChr = NULL) {
  if (length(v) == 0) return(character(0))

  if (is.null(endquoteChr)) endquoteChr <- quoteChr

  paste0(quoteChr, v, endquoteChr)
}

##' Print a text for English prosa
##'
##' Pastes a vector and adds comma and "and" to the correct places
##' @title makeEnglishList
##' @param v vector
##' @param sep character. spearates all but last entries of \code{v}
##'   [", "]
##' @param lastsep character. spearates the last entries of \code{v}
##'   [", and "]
##' @param onlysep character. spearates the two entries of \code{v} if
##'   \code{length(v) == 2} [" and "]
##' @return character with plain text English prosa version
##' @author Andreas Leha
##' @export
##' @examples
##' ## defaut separators
##' makeEnglishList(c("foo", "bar", "baz"))
##' makeEnglishList(c("foo", "bar"))
##' makeEnglishList(c("foo"))
##'
##' ## without the 'Oxford comma'
##' makeEnglishList(c("foo", "bar", "baz"), lastsep = " and ")
##'
##' ## an 'or' list
##' makeEnglishList(c("foo", "bar", "baz"), lastsep = ", or ")
makeEnglishList <- function(v, sep = ", ", lastsep = ", and ", onlysep = " and ") {
  l <- length(v)

  if (l == 0)
    return("")

  if (l == 1)
    return(v)

  if (l == 2)
    return(paste(v, collapse = onlysep))

  ret <- paste(v[1:(l-1)], collapse=sep)
  ret <- paste(ret, v[l], sep=paste0(lastsep))
  ret
}

##' English text version of number
##'
##' Converts a number to a text version of that number
##' @title digits2text
##' @param x number to convert
##' @param mult to be appended (like a unit)
##' @return character
##' @author Graham Williams \email{Graham.Williams@@togawre.com}
##' \url{http://rattle.togaware.com/utility.R}
##' @export
digits2text <- function(x,mult="") {
  if (x == 0) return("zero")

  if (!requireNamespace("stringr"))
    stop("digits2text requires stringr")

  units <- c("one","two","three","four","five",
           "six","seven","eight","nine")
  teens <- c("ten","eleven","twelve","thirteen","fourteen",
           "fifteen","sixteen","seventeen","eighteen","nineteen")
  tens <- c("ten","twenty","thirty","forty","fifty",
          "sixty","seventy","eighty","ninety")

  digits <- rev(as.numeric(strsplit(as.character(x),"")[[1]]))
  digilen <- length(digits)

  if(digilen == 2 && digits[2] == 1) return(teens[digits[1]+1])

  digitext <- units[digits[1]]
  if(digilen > 1) digitext <- c(digitext, tens[digits[2]])
  if(digilen > 2) digitext <- c(digitext, "hundred", units[digits[3]])
  if(digilen > 3) digitext <- c(digitext,
                                digits2text(floor(x/1000),"thousand"))
  if(digilen > 6) digitext <- c(digitext,
                                digits2text(floor(x/1000000),"million"))

  return(stringr::str_trim(paste(c(rev(digitext),mult),sep="",collapse=" ")))
}

convertColumnHeading <- function(df, col, movealong=NULL)
{
  df[[col]] <- factor(df[[col]], levels=unique(df[[col]]))

  ddply(df, col, function(x) {

    ## don't do anything if there is only one row
    if (nrow(x) == 1) return(x)

    ## extract the 'heading' column
    heading <- x[[col]]
    heading <- as.character(heading[1])

    ## remove the 'heading' column
    x <- x[,colnames(x) != col, drop=FALSE]

    ## add an empty row at the top
    x <- rbind("", x)

    ## add the 'heading' column back
    x <- cbind(c(heading, rep("", nrow(x)-1)),
                x)

    ## fix the colnames
    colnames(x)[1] <- col

    ## move along
    if (!is.null(movealong))
      for (mcol in movealong)
        x[[mcol]] <- c(x[[mcol]][-1], x[[mcol]][1])

    ## return the data.frame
    return(x)
  })
}

##' change the description into the long format
##'
##' only for internal use.
##' @title calc_descr_long
##' @param x vector of which to calc the descriptive values
##' @param xname the name of the variable encoded in x
##' @param includeNAs boolean. include the number of NAs in the output?
##' @param factorlevellimit integer.  for factors with more than
##'   \code{factorlevellimit} levels, not all levels are printed
##' @return data.frame of character describing x
##' @author Andreas Leha
##' @export
##' @keywords internal
calc_descr_long_old <- function(x, xname, includeNAs=FALSE, orderedAsUnordered=FALSE, factorlevellimit = 14) {
  ttt <- calc_descr(x, xname, includeNAs=TRUE, orderedAsUnordered=orderedAsUnordered, factorlevellimit = factorlevellimit, descr_digits = 2, significant_digits = TRUE)

  if (ttt[1,2] != "") {
    ttt2 <- ttt[1,]
    ttt2[1,1] <- "\\vert mean \\pm sd"
    ttt <- rbind(ttt, ttt2)
    ttt[1,2] <- ""
  }
  if (ttt[1,3] != "") {
    ttt3 <- ttt[1,]
    ttt3[1,1] <- "\\vert median (min; max)"
    ttt3[1,2] <- ttt[1,3]
    ttt <- rbind(ttt, ttt3)
  }
  if (includeNAs) {
    ttt4 <- ttt[1,]
    ttt4[1,1] <- "\\vert missing"
    ttt4[1,2] <- paste(ttt[1,4])
    ttt <- rbind(ttt, ttt4)
  }

  ttt[,1:2]
}

##' change the description into the long format
##'
##' only for internal use.
##' @title calc_descr_long
##' @param x vector of which to calc the descriptive values
##' @param xname the name of the variable encoded in x
##' @param extraLevels named list of lists.  Names have to be variable names.  Elements have to have to be named list of this form: \code{`some label` = list(idxvec = idxvec, display = logical)}.  Here \code{idxvec} needs to be a logical vector of length \code{nrow(df)} that specifies the affected rows.  If \code{display} is \code{TRUE} the number of affected rows will be shown under \code{some label}.
##' @param includeNAs logical. include the number of NAs in the output?
##' @param orderedAsUnordered logical. treat ordered factors as
##'   unordered factors?
##' @param factorlevellimit integer.  for factors with more than
##'   \code{factorlevellimit} levels, not all levels are printed
##' @param show.minmax logical. if TRUE show minimum and maximum for numeric variables.  Defaults to \code{TRUE}.
##' @param show.IQR logical. if TRUE show 25\% and 75\% quantiles for numeric variables.  Defaults to \code{FALSE}.
##' @param sd_digits character.  one of c("by_mean", "fixed").  If 'by_mean', the number of decimal places of the standard deviation is limited by the number of decimal places of the mean.
##' @param descr_digits integer. Number of digits for formatting of descriptive values. Defaults to 2.
##' @param significant_digits boolean.  if TRUE, the number of significant digits of is given by descr_digits. Otherwise the number of decimal places is fixed.
##' @return data.frame of character describing x
##' @author Andreas Leha
##' @export
##' @keywords internal
calc_descr_long <- function(x,
                            xname,
                            extraLevels = NULL,
                            includeNAs=FALSE,
                            orderedAsUnordered=FALSE,
                            factorlevellimit = 14,
                            show.minmax = TRUE,
                            show.IQR = FALSE,
                            sd_digits = "by_mean",
                            descr_digits = 2,
                            significant_digits = TRUE) {

  ttt <- calc_descr(x,
                    xname,
                    includeNAs=TRUE,
                    extraLevels = extraLevels,
                    orderedAsUnordered=orderedAsUnordered,
                    factorlevellimit = factorlevellimit,
                    show.minmax = show.minmax,
                    show.IQR = show.IQR,
                    sd_digits = sd_digits,
                    descr_digits = descr_digits,
                    significant_digits = significant_digits)

  missing_cols <- grep("^\\.\\.missing\\.\\.", colnames(ttt), value = TRUE)
  missing_labels <- gsub("^\\.\\.missing\\.\\.missing$", "\\\\vert missing", missing_cols)  ## we keep \\vert as 'marker'
  missing_labels <- gsub("^\\.\\.missing\\.\\.", "", missing_labels)  ## we keep \\vert as 'marker'

  ## convert into data.frame
  if (nrow(ttt) == 1 && !grepl("/\\.\\.\\./", ttt[1,2])) {
    missing_values <- ttt[1, missing_cols]
    res <- data.frame(parameter = ttt["parameter"],
                      level = c("mean \\pm sd",    ## \\pm might be replaced at the end
                                ifelse(show.minmax && show.IQR,
                                       "median (min; max; [IQR])",
                                       ifelse(show.minmax && !show.IQR,
                                              "median (min; max)",
                                              ifelse(!show.minmax && show.IQR,
                                                     "median [IQR]",
                                                     ifelse(!show.minmax && !show.IQR,
                                                            "median",
                                                            "")))),
                              missing_labels),
                      value = unname(c(unlist(ttt[1, 2:3]), unlist(ttt[1, missing_cols]))),
                      stringsAsFactors = FALSE)
  } else if (nrow(ttt) == 1 && grepl("/\\.\\.\\./", ttt[1,2])) {
    res <- data.frame(parameter = ttt["parameter"],
                      level = c("",    ## \\pm might be replaced at the end
                                "",
                                missing_labels), ## we keep \\vert as 'marker'
                      value = unname(c(unlist(ttt[1, 2:3]), unlist(ttt[1, missing_cols]))),
                      stringsAsFactors = FALSE)
  } else {
    res <- data.frame(parameter = ttt[1,1],
                      level = c(gsub("\\\\vert[[:space:]]*",
                                     "",
                                     ttt[-1, 1]),
                                missing_labels), ## we keep \\vert as 'marker'
                      value = c(ttt[-1, 2], unname(unlist(ttt[1,missing_cols]))),
                      stringsAsFactors = FALSE)
  }

  ## 'nice' EXCEL-style data.frame
  if (FALSE)
    convertColumnHeading(res, "parameter")

  ## remove rows w/o values
  res <- res[res$value != "",,drop = FALSE]

  ## remove the information on missing values if requested
  if (!includeNAs) {
    res <- res[res$level != "\\vert missing",,drop = FALSE]
  }

  return(res)
}

# compute the number of decimal places for a string representing a number
decimalplaces <- function(x) {
  if (grepl( ".", x, fixed = TRUE)) {
    nchar(gsub("^.*\\.","",x))
  } else {
    return(0)
  }
}

formatC_mod <- function(x, digits = NULL, format = NULL, ...){
  res <- formatC(x = x, digits = digits, format = format, ...)
  for(i in 1:length(res))
  {
    if(format=='fg' & res[i]=="0")
       res[i] <- formatC(x = x[i], digits = max(digits-1,0), format = 'f', ...)
    if(decimalplaces(res[i])==0) {
      res[i] <- sub("\\.", "", res[i])
      } else{
      if(grepl("\\.e\\+", res[i]))
         res[i] <- as.character(as.numeric(res[i]))
      }
  }
  return(res)
}

countNAs <- function(x, xname, extraLevels = NULL)
{
  usedcases <- rep(FALSE, length(x))

  if (any(sapply(extraLevels, function(el) any(is.na(el$idxvec)))))
    stop("missing values in extraLevels for ", xname, ".")

  if (!is.null(extraLevels)) {
    if (length(extraLevels) > 1)
      warning("multiple extraLevels for ", xname, "\n",
              "these will be applied in order ",
              "(i.e. cases in levels in at the front of the list will be counted, ",
              "if the same cases are also in levels at the end of the list ",
              "they will not be counted for these levels.")

    for (i in 1:length(extraLevels)) {
      extraLevels[[i]]$n <- sum(extraLevels[[i]]$idxvec & !usedcases)
      usedcases <- usedcases | extraLevels[[i]]$idxvec
      x[extraLevels[[i]]$idxvec] <- NA
    }
    res <- setNames(sapply(extraLevels, function(x) x$n), paste0("..missing..", names(extraLevels)))
    idx_display <- sapply(extraLevels, function(xi) xi$display)
    res <- res[idx_display, drop = FALSE]
    res <- as_tibble(as.list(res))
  } else {
    res <- NULL
  }

  nnas <- setNames(sum(is.na(x) & !usedcases), "..missing..missing")
  nnas <- as_tibble(as.list(nnas))

  if (is.null(res) || nrow(res) == 0) {
    res <- nnas
  } else {
    res <- cbind(res, nnas)
  }

  return(list(nNAs = as.data.frame(res),
              x = x))
}



##' calc descriptive values
##'
##' create a descriptive values containing data.frame.
##' @title calc_descr
##' @param x vector of which to calc the descriptive values
##' @param xname name of the varible holding values in x
##' @param extraLevels named list of lists.  Names have to be variable names.  Elements have to have to be named list of this form: \code{`some label` = list(idxvec = idxvec, display = logical)}.  Here \code{idxvec} needs to be a logical vector of length \code{nrow(df)} that specifies the affected rows.  If \code{display} is \code{TRUE} the number of affected rows will be shown under \code{some label}.
##' @param includeNAs boolean. include the number of NAs in the output?
##' @param orderedAsUnordered logical. treat ordered factors as
##'   unordered factors?
##' @param factorlevellimit integer.  for factors with more than
##'   \code{factorlevellimit} levels, not all levels are printed
##' @param show.minmax logical. if TRUE show minimum and maximum for numeric variables.  Defaults to \code{TRUE}.
##' @param show.IQR logical. if TRUE show 25\% and 75\% quantiles for numeric variables.  Defaults to \code{FALSE}.
##' @param sd_digits character.  one of c("by_mean", "fixed").  If 'by_mean', the number of decimal places of the standard deviation is limited by the number of decimal places of the mean.
##' @param descr_digits integer. Number of digits for formatting of descriptive values. Defaults to 2.
##' @param significant_digits boolean.  if TRUE, the number of significant digits of is given by descr_digits. Otherwise the number of decimal places is fixed.
##' @return data.frame of character describing x
##' @author Andreas Leha
##' @export
##' @keywords internal
calc_descr <- function(x,
                       xname,
                       extraLevels = NULL,
                       includeNAs=TRUE,
                       orderedAsUnordered=FALSE,
                       factorlevellimit = 14,
                       show.minmax = TRUE,
                       show.IQR = FALSE,
                       sd_digits = "by_mean",
                       descr_digits = 2,
                       significant_digits = TRUE) {
  ## count NAs, numbers in extraLevels, and
  ## make sure the extraLevels are NA for all calculations
  nnas <- countNAs(x = x, xname = xname, extraLevels = extraLevels)
  x <- nnas$x

  ## to be used below
  emptyNAs <- nnas$nNAs
  emptyNAs[1,] <- ""


  if (all(is.na(x))) {
    if (is.factor(x) && !is.ordered(x) && !(length(levels(x)) > factorlevellimit)) {
      retttt <- data.frame(parameter=as.character(xname),
                           `mean \\pm sd`="",
                           `median (minimum, maximum)`="",
                           stringsAsFactors=FALSE)
      retttt <- cbind(retttt, nnas$nNAs)
      for (i in 1:length(table(x))) {
        resi <- data.frame(parameter=paste("\\vert ",
                                           names(table(x))[i], sep=""),
                           `mean \\pm sd`="",
                           `median (minimum, maximum)`="",
                           stringsAsFactors=FALSE)
        resi <- cbind(resi, emptyNAs)
        retttt <- rbind(retttt, resi)
      }
      res <- retttt
    } else {
      res <- data.frame(parameter=as.character(xname),
                        `mean \\pm sd`="",
                        `median (minimum, maximum)`="",
                        stringsAsFactors=FALSE)
      res <- cbind(res, nnas$nNAs)
    }
  } else {
    if (is.factor(x)) {
      if (is.ordered(x) && !orderedAsUnordered) {
        if (!any(is.na(suppressWarnings(as.numeric(as.character(x[!is.na(x)])))))) { ## score like factors
          decimalplaces_mean <- decimalplaces(formatC_mod(mean(as.numeric(as.character(x)),
                                                               na.rm=TRUE),
                                                          format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                          digits=descr_digits))
          res <- data.frame(parameter=as.character(xname),
                            `mean \\pm sd`=paste(formatC_mod(mean(as.numeric(as.character(x)),
                                                                  na.rm=TRUE),
                                                             format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                             digits=descr_digits),
                                                 formatC_mod(round(sd(as.numeric(as.character(x)),
                                                                      na.rm=TRUE), ifelse(sd_digits=="by_mean",decimalplaces_mean,Inf)),
                                                             format = 'f' ,
                                                             digits= ifelse(sd_digits=="by_mean",decimalplaces_mean,descr_digits)),
                                                 sep=" \\pm "),
                            `median (minimum, maximum)`=
                              if (show.minmax && !show.IQR) {
                                paste(
                                  formatC_mod(median(as.numeric(as.character(x)), na.rm=TRUE),
                                              format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                              digits=descr_digits),
                                  paste("(",
                                        formatC_mod(min(as.numeric(as.character(x)), na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        "; ",
                                        formatC_mod(max(as.numeric(as.character(x)), na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        ")",sep = ""))
                              } else if (show.minmax && show.IQR) {
                                paste(
                                  formatC_mod(median(as.numeric(as.character(x)), na.rm=TRUE),
                                              format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                              digits=descr_digits),
                                  paste("(",
                                        formatC_mod(min(as.numeric(as.character(x)), na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        "; ",
                                        formatC_mod(max(as.numeric(as.character(x)), na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        "; ",
                                        formatIQR(as.numeric(as.character(x)), na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits),
                                        ")",sep = ""))
                              } else if (!show.minmax && show.IQR) {
                                paste(
                                  formatC_mod(median(as.numeric(as.character(x)), na.rm=TRUE),
                                              format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                              digits=descr_digits),
                                  formatIQR(as.numeric(as.character(x)), na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits))
                              } else if (!show.minmax && !show.IQR) {
                                paste(
                                  formatC_mod(median(as.numeric(as.character(x)), na.rm=TRUE), format = ifelse(significant_digits, 'fg', 'f') ,flag = "#", digits=descr_digits))
                              },
                            ##..missing..missing=sum(is.na(x)),
                            stringsAsFactors=FALSE)
          res <- cbind(res, nnas$nNAs)
        } else {
          res <- data.frame(parameter=as.character(xname),
                            `mean \\pm sd`="",
                            `median (minimum, maximum)`=
                              if (show.minmax && !show.IQR) {
                                paste(median(x, na.rm=TRUE),
                                      paste("(",
                                            min(x, na.rm=TRUE),
                                            "; ",
                                            max(x, na.rm=TRUE),
                                            ")",sep = ""))
                              } else if (show.minmax && show.IQR) {
                                paste(median(x, na.rm=TRUE),
                                      paste("(",
                                            min(x, na.rm=TRUE),
                                            "; ",
                                            max(x, na.rm=TRUE),
                                            "; ",
                                            formatIQR(x, na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits),
                                            ")",sep = ""))
                              } else if (!show.minmax && show.IQR) {
                                paste(
                                  median(x, na.rm=TRUE),
                                  formatIQR(x, na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits))
                              } else if (!show.minmax && !show.IQR) {
                                paste(
                                  median(x, na.rm=TRUE))
                              },
                            stringsAsFactors=FALSE)
          res <- cbind(res, nnas$nNAs)
        }
      } else if (length(levels(x)) > factorlevellimit) {      ## too many levels to plot
        res <- data.frame(parameter=as.character(xname),
                          `mean \\pm sd`=paste(names(table(x))[1],
                            "/.../",
                            names(rev(table(x)))[1],sep=""),
                          `median (minimum, maximum)`="",
                          stringsAsFactors=FALSE)
        res <- cbind(res, nnas$nNAs)
      } else {                                  ## factors
        ##res <- data.frame(parameter=as.character(xname),
        ##           `mean \\pm sd`=paste(paste(table(x), collapse="/"),
        ##             paste("(",
        ##                   paste(round(table(x)/sum(table(x))*100),
        ##                         "%", sep="", collapse="/"),
        ##                   ")",
        ##                   sep="")),
        ##           `median (minimum, maximum)`="",
        ##           NAs=sum(is.na(x)),
        ##           stringsAsFactors=FALSE)
        retttt <- data.frame(parameter=as.character(xname),
                             `mean \\pm sd`="",
                             `median (minimum, maximum)`="",
                             ##NAs=paste0(sum(is.na(x)), " (", format(sum(is.na(x)/length(x)), digits=1, nsmall=1), "%)"),
                             ##NAs=sum(is.na(x)),
                             stringsAsFactors=FALSE)
        retttt <- cbind(retttt, nnas$nNAs)
        for (i in 1:length(table(x))) {
          resi <- data.frame(parameter=paste("\\vert ",
                                             names(table(x))[i], sep=""),
                             `mean \\pm sd`=paste(table(x)[i],
                                                  paste("(",
                                                        gsub("[[:blank:]]", "", format(round((table(x)/(sum(table(x)))*100)[i], digits=1), nsmall=1)),
                                                        "%",
                                                        ")",
                                                        sep="")),
                             `median (minimum, maximum)`="",
                             stringsAsFactors=FALSE)
          resi <- cbind(resi, emptyNAs)
          retttt <- rbind(retttt, resi)
        }
        res <- retttt
      }
    } else if (is.numeric(x)) {                 ## numerics
      decimalplaces_mean <- decimalplaces(formatC_mod(mean(x, na.rm=TRUE),
                                                      format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                      digits=descr_digits))
      res <-
        data.frame(parameter=as.character(xname),
                   `mean \\pm sd`=paste(formatC_mod(mean(x, na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        formatC_mod(round(sd(x, na.rm=TRUE),ifelse(sd_digits=="by_mean",decimalplaces_mean,Inf)),
                                                    format = 'f',
                                                    digits= ifelse(sd_digits=="by_mean",decimalplaces_mean,descr_digits)),
                                        sep=" \\pm "),
                   `median (minimum, maximum)`=
                     if (show.minmax && !show.IQR) {
                       paste(
                         formatC_mod(median(x, na.rm=TRUE),
                                     format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                     digits=descr_digits),
                         paste("(",
                               formatC_mod(min(x, na.rm=TRUE),
                                           format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                           digits=descr_digits),
                               "; ",
                               formatC_mod(max(x, na.rm=TRUE),
                                           format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                           digits=descr_digits),
                               ")",sep = ""))
                     } else if (show.minmax && show.IQR) {
                       paste(
                         formatC_mod(median(x, na.rm=TRUE),
                                     format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                     digits=descr_digits),
                         paste("(",
                               formatC_mod(min(x, na.rm=TRUE),
                                           format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                           digits=descr_digits),
                               "; ",
                               formatC_mod(max(x, na.rm=TRUE),
                                           format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                           digits=descr_digits),
                               "; ",
                               formatIQR(x, na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits),
                               ")",sep = ""))
                     } else if (!show.minmax && show.IQR) {
                       paste(
                         formatC_mod(median(x, na.rm=TRUE),
                                     format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                     digits=descr_digits),
                         formatIQR(x, na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits))
                     } else if (!show.minmax && !show.IQR) {
                       paste(
                         formatC_mod(median(x, na.rm=TRUE), format = ifelse(significant_digits, 'fg', 'f') ,flag = "#", digits=descr_digits))
                     },
                   stringsAsFactors=FALSE) %>%
        cbind(nnas$nNAs)
    } else if (is.tod(x)) {                 ## time of day
      res <-
        data.frame(parameter=as.character(xname),
                   `mean \\pm sd`=paste(mean(x, na.rm=TRUE),
                                        sd(x, na.rm=TRUE),
                                        sep=" \\pm "),
                   `median (minimum, maximum)`=
                     if (show.minmax && !show.IQR) {
                       paste(median(x, na.rm=TRUE),
                             paste("(",
                               min(x, na.rm=TRUE),
                               "; ",
                               max(x, na.rm=TRUE),
                               ")",sep = ""))
                     } else if (show.minmax && show.IQR) {
                       paste(
                         median(x, na.rm=TRUE),
                         paste("(",
                               min(x, na.rm=TRUE),
                               "; ",
                               max(x, na.rm=TRUE),
                               "; ",
                               formatIQR(x, na.rm=TRUE),
                               ")",sep = ""))
                     } else if (!show.minmax && show.IQR) {
                       paste(
                         median(x, na.rm=TRUE),
                         formatIQR(x, na.rm=TRUE))
                     } else if (!show.minmax && !show.IQR) {
                       paste(
                         median(x, na.rm=TRUE))
                     },
                   stringsAsFactors=FALSE)
              res <- cbind(res, nnas$nNAs)
    } else if (inherits(x, c("Date", "POSIXt"), which = FALSE)) {            ## dates
      res <- data.frame(parameter=as.character(xname),
                        `mean \\pm sd`=as.character(mean(x, na.rm=TRUE)),
                        `median (minimum, maximum)`=paste(format(median(x,
                                                                        na.rm=TRUE), digits=2),
                                                          paste("(",
                                                                format(min(x, na.rm=TRUE), digits=2),
                                                                "; ",
                                                                format(max(x, na.rm=TRUE), digits=2),
                                                                ")",sep = "")),
                        stringsAsFactors=FALSE)
      res <- cbind(res, nnas$nNAs)
    } else {                                    ## empty row otherwise
      res <- data.frame(parameter=as.character(xname),
                        `mean \\pm sd`="",
                        `median (minimum, maximum)`="",
                        stringsAsFactors=FALSE)
      res <- cbind(res, emptyNAs)
    }
  }
  if (includeNAs) {
    res
  } else {
    res[,-ncol(res)]
  }
}



##' calc descriptive values
##'
##' create a descriptive values containing data.frame.
##' @title calc_descr
##' @param x vector of which to calc the descriptive values
##' @param xname name of the varible holding values in x
##' @param extraLevels named list of lists.  Names have to be variable names.  Elements have to have to be named list of this form: \code{`some label` = list(idxvec = idxvec, display = logical)}.  Here \code{idxvec} needs to be a logical vector of length \code{nrow(df)} that specifies the affected rows.  If \code{display} is \code{TRUE} the number of affected rows will be shown under \code{some label}.
##' @param includeNAs boolean. include the number of NAs in the output?
##' @param orderedAsUnordered logical. treat ordered factors as
##'   unordered factors?
##' @param factorlevellimit integer.  for factors with more than
##'   \code{factorlevellimit} levels, not all levels are printed
##' @param show.minmax logical. if TRUE show minimum and maximum for numeric variables.  Defaults to \code{TRUE}.
##' @param show.IQR logical. if TRUE show 25\% and 75\% quantiles for numeric variables.  Defaults to \code{FALSE}.
##' @param sd_digits character.  one of c("by_mean", "fixed").  If 'by_mean', the number of decimal places of the standard deviation is limited by the number of decimal places of the mean.
##' @param descr_digits integer. Number of digits for formatting of descriptive values. Defaults to 2.
##' @param significant_digits boolean.  if TRUE, the number of significant digits of is given by descr_digits. Otherwise the number of decimal places is fixed.
##' @return data.frame of character describing x
##' @author Andreas Leha
##' @export
##' @keywords internal
calc_descr_old <- function(x,
                       xname,
                       extraLevels = NULL,
                       includeNAs=TRUE,
                       orderedAsUnordered=FALSE,
                       factorlevellimit = 14,
                       show.minmax = TRUE,
                       show.IQR = FALSE,
                       sd_digits = "by_mean",
                       descr_digits = 2,
                       significant_digits = TRUE) {
  ## count NAs, numbers in extraLevels, and
  ## make sure the extraLevels are NA for all calculations
  nnas <- countNAs(x = x, xname = xname, extraLevels = extraLevels)
  x <- nnas$x
  if (all(is.na(x))) {
    if (is.factor(x) && !is.ordered(x) && !(length(levels(x)) > factorlevellimit)) {
      retttt <- data.frame(parameter=as.character(xname),
                           `mean \\pm sd`="",
                           `median (minimum, maximum)`="",
                           NAs=length(x),
                           stringsAsFactors=FALSE)
      for (i in 1:length(table(x))) {
        retttt <-
          rbind(retttt,
                data.frame(parameter=paste("\\vert ",
                                           names(table(x))[i], sep=""),
                           `mean \\pm sd`="",
                           `median (minimum, maximum)`="",
                           NAs="",
                           stringsAsFactors=FALSE))
      }
      res <- retttt
    } else {
      res <- data.frame(parameter=as.character(xname),
                        `mean \\pm sd`="",
                        `median (minimum, maximum)`="",
                        NAs=length(x),
                        stringsAsFactors=FALSE)
    }
  } else {
    if (is.factor(x)) {
      if (is.ordered(x) && !orderedAsUnordered) {
        if (!any(is.na(suppressWarnings(as.numeric(as.character(x[!is.na(x)])))))) { ## score like factors
          decimalplaces_mean <- decimalplaces(formatC_mod(mean(as.numeric(as.character(x)),
                                                               na.rm=TRUE),
                                                          format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                          digits=descr_digits))
          res <- data.frame(parameter=as.character(xname),
                            `mean \\pm sd`=paste(formatC_mod(mean(as.numeric(as.character(x)),
                                                                  na.rm=TRUE),
                                                             format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                             digits=descr_digits),
                                                 formatC_mod(round(sd(as.numeric(as.character(x)),
                                                                      na.rm=TRUE), ifelse(sd_digits=="by_mean",decimalplaces_mean,Inf)),
                                                             format = 'f' ,
                                                             digits= ifelse(sd_digits=="by_mean",decimalplaces_mean,descr_digits)),
                                                 sep=" \\pm "),
                            `median (minimum, maximum)`=
                              if (show.minmax && !show.IQR) {
                                paste(
                                  formatC_mod(median(as.numeric(as.character(x)), na.rm=TRUE),
                                              format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                              digits=descr_digits),
                                  paste("(",
                                        formatC_mod(min(as.numeric(as.character(x)), na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        "; ",
                                        formatC_mod(max(as.numeric(as.character(x)), na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        ")",sep = ""))
                              } else if (show.minmax && show.IQR) {
                                paste(
                                  formatC_mod(median(as.numeric(as.character(x)), na.rm=TRUE),
                                              format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                              digits=descr_digits),
                                  paste("(",
                                        formatC_mod(min(as.numeric(as.character(x)), na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        "; ",
                                        formatC_mod(max(as.numeric(as.character(x)), na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        "; ",
                                        formatIQR(as.numeric(as.character(x)), na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits),
                                        ")",sep = ""))
                              } else if (!show.minmax && show.IQR) {
                                paste(
                                  formatC_mod(median(as.numeric(as.character(x)), na.rm=TRUE),
                                              format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                              digits=descr_digits),
                                  formatIQR(as.numeric(as.character(x)), na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits))
                              } else if (!show.minmax && !show.IQR) {
                                paste(
                                  formatC_mod(median(as.numeric(as.character(x)), na.rm=TRUE), format = ifelse(significant_digits, 'fg', 'f') ,flag = "#", digits=descr_digits))
                              },
                            NAs=sum(is.na(x)),
                            stringsAsFactors=FALSE)
        } else {
          res <- data.frame(parameter=as.character(xname),
                            `mean \\pm sd`="",
                            `median (minimum, maximum)`=paste(median(x, na.rm=TRUE),
                                                              paste("(",
                                                                    min(x, na.rm=TRUE),
                                                                    "; ",
                                                                    max(x, na.rm=TRUE),
                                                                    ")",sep = "")),
                            NAs=sum(is.na(x)),
                            stringsAsFactors=FALSE)
        }
      } else if (length(levels(x)) > factorlevellimit) {      ## too many levels to plot
        res <- data.frame(parameter=as.character(xname),
                          `mean \\pm sd`=paste(names(table(x))[1],
                            "/.../",
                            names(rev(table(x)))[1],sep=""),
                          `median (minimum, maximum)`="",
                          NAs=sum(is.na(x)),
                          stringsAsFactors=FALSE)
      } else {                                  ## factors
        ##res <- data.frame(parameter=as.character(xname),
        ##           `mean \\pm sd`=paste(paste(table(x), collapse="/"),
        ##             paste("(",
        ##                   paste(round(table(x)/sum(table(x))*100),
        ##                         "%", sep="", collapse="/"),
        ##                   ")",
        ##                   sep="")),
        ##           `median (minimum, maximum)`="",
        ##           NAs=sum(is.na(x)),
        ##           stringsAsFactors=FALSE)
        retttt <- data.frame(parameter=as.character(xname),
                             `mean \\pm sd`="",
                             `median (minimum, maximum)`="",
                             ##NAs=paste0(sum(is.na(x)), " (", format(sum(is.na(x)/length(x)), digits=1, nsmall=1), "%)"),
                             NAs=sum(is.na(x)),
                             stringsAsFactors=FALSE)
        for (i in 1:length(table(x))) {
          retttt <-
            rbind(retttt,
                  data.frame(parameter=paste("\\vert ",
                                             names(table(x))[i], sep=""),
                             `mean \\pm sd`=paste(table(x)[i],
                                                  paste("(",
                                                        gsub("[[:blank:]]", "", format(round((table(x)/(sum(table(x)))*100)[i], digits=1), nsmall=1)),
                                                        "%",
                                                        ")",
                                                        sep="")),
                             `median (minimum, maximum)`="",
                             NAs="",
                             stringsAsFactors=FALSE))
        }
        res <- retttt
      }
    } else if (is.numeric(x)) {                 ## numerics
      decimalplaces_mean <- decimalplaces(formatC_mod(mean(x, na.rm=TRUE),
                                                      format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                      digits=descr_digits))
      res <-
        data.frame(parameter=as.character(xname),
                   `mean \\pm sd`=paste(formatC_mod(mean(x, na.rm=TRUE),
                                                    format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                                    digits=descr_digits),
                                        formatC_mod(round(sd(x, na.rm=TRUE),ifelse(sd_digits=="by_mean",decimalplaces_mean,Inf)),
                                                    format = 'f',
                                                    digits= ifelse(sd_digits=="by_mean",decimalplaces_mean,descr_digits)),
                                        sep=" \\pm "),
                   `median (minimum, maximum)`=
                     if (show.minmax && !show.IQR) {
                       paste(
                         formatC_mod(median(x, na.rm=TRUE),
                                     format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                     digits=descr_digits),
                         paste("(",
                               formatC_mod(min(x, na.rm=TRUE),
                                           format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                           digits=descr_digits),
                               "; ",
                               formatC_mod(max(x, na.rm=TRUE),
                                           format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                           digits=descr_digits),
                               ")",sep = ""))
                     } else if (show.minmax && show.IQR) {
                       paste(
                         formatC_mod(median(x, na.rm=TRUE),
                                     format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                     digits=descr_digits),
                         paste("(",
                               formatC_mod(min(x, na.rm=TRUE),
                                           format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                           digits=descr_digits),
                               "; ",
                               formatC_mod(max(x, na.rm=TRUE),
                                           format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                           digits=descr_digits),
                               "; ",
                               formatIQR(x, na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits),
                               ")",sep = ""))
                     } else if (!show.minmax && show.IQR) {
                       paste(
                         formatC_mod(median(x, na.rm=TRUE),
                                     format = ifelse(significant_digits, 'fg', 'f') ,flag = "#",
                                     digits=descr_digits),
                         formatIQR(x, na.rm=TRUE, digits=descr_digits, significant_digits=significant_digits))
                     } else if (!show.minmax && !show.IQR) {
                       paste(
                         formatC_mod(median(x, na.rm=TRUE), format = ifelse(significant_digits, 'fg', 'f') ,flag = "#", digits=descr_digits))
                     },
                   NAs=sum(is.na(x)),
                   stringsAsFactors=FALSE)
    } else if (is.tod(x)) {                 ## time of day
      res <-
        data.frame(parameter=as.character(xname),
                   `mean \\pm sd`=paste(mean(x, na.rm=TRUE),
                                        sd(x, na.rm=TRUE),
                                        sep=" \\pm "),
                   `median (minimum, maximum)`=
                     if (show.minmax && !show.IQR) {
                       paste(median(x, na.rm=TRUE),
                             paste("(",
                               min(x, na.rm=TRUE),
                               "; ",
                               max(x, na.rm=TRUE),
                               ")",sep = ""))
                     } else if (show.minmax && show.IQR) {
                       paste(
                         median(x, na.rm=TRUE),
                         paste("(",
                               min(x, na.rm=TRUE),
                               "; ",
                               max(x, na.rm=TRUE),
                               "; ",
                               formatIQR(x, na.rm=TRUE),
                               ")",sep = ""))
                     } else if (!show.minmax && show.IQR) {
                       paste(
                         median(x, na.rm=TRUE),
                         formatIQR(x, na.rm=TRUE))
                     } else if (!show.minmax && !show.IQR) {
                       paste(
                         median(x, na.rm=TRUE))
                     },
                   NAs=sum(is.na(x)),
                   stringsAsFactors=FALSE)
    } else if (inherits(x, c("Date", "POSIXt"), which = FALSE)) {            ## dates
      res <- data.frame(parameter=as.character(xname),
                        `mean \\pm sd`=as.character(mean(x, na.rm=TRUE)),
                        `median (minimum, maximum)`=paste(format(median(x,
                                                                        na.rm=TRUE), digits=2),
                                                          paste("(",
                                                                format(min(x, na.rm=TRUE), digits=2),
                                                                "; ",
                                                                format(max(x, na.rm=TRUE), digits=2),
                                                                ")",sep = "")),
                        NAs=sum(is.na(x)),
                        stringsAsFactors=FALSE)
    } else {                                    ## empty row otherwise
      res <- data.frame(parameter=as.character(xname),
                        `mean \\pm sd`="",
                        `median (minimum, maximum)`="",
                        NAs="",
                        stringsAsFactors=FALSE)
    }
  }
  if (includeNAs) {
    res
  } else {
    res[,-ncol(res)]
  }
}

##' Wrapper for single vectors
##'
##' calls the one dimensional functions
##' @title calc_descr_matrix
##' @param ttt the data.frame
##' @param format in c("long", "wide")
##' @param extraLevels named list of lists.  Names have to be variable names.  Elements have to have to be named list of this form: \code{`some label` = list(idxvec = idxvec, display = logical)}.  Here \code{idxvec} needs to be a logical vector of length \code{nrow(df)} that specifies the affected rows.  If \code{display} is \code{TRUE} the number of affected rows will be shown under \code{some label}.
##' @param includeNAs boolean. include number of NAs in the output?
##' @param orderedAsUnordered logical. treat ordered factors as
##'   unordered factors?
##' @param factorlevellimit integer.  for factors with more than
##'   \code{factorlevellimit} levels, not all levels are printed
##' @param show.minmax logical. if TRUE show minimum and maximum for numeric variables.  Defaults to \code{TRUE}.
##' @param show.IQR logical. if TRUE show 25\% and 75\% quantiles for numeric variables.  Defaults to \code{FALSE}.
##' @param sd_digits character.  one of c("by_mean", "fixed").  If 'by_mean', the number of decimal places of the standard deviation is limited by the number of decimal places of the mean.
##' @param descr_digits integer. Number of digits for formatting of descriptive values. Defaults to 2.
##' @param significant_digits boolean.  if TRUE, the number of significant digits of is given by descr_digits. Otherwise the number of decimal places is fixed.
##' @return matrix containing the descriptive values
##' @author Andreas Leha
calc_descr_matrix <- function(ttt,
                              format="long",
                              extraLevels = NULL,
                              includeNAs=FALSE,
                              orderedAsUnordered=FALSE,
                              factorlevellimit = 14,
                              show.minmax = TRUE,
                              show.IQR = FALSE,
                              sd_digits ="by_mean",
                              descr_digits = 2,
                              significant_digits = TRUE) {

  ## silence R CMD CHECK
  i <- NULL

  ##tttt <- ldply(ttt, .fun=calc_descr)
  if (format=="wide") {
    tttt <- foreach(i=1:length(ttt), .combine=rbind) %do% {
      calc_descr(ttt[[i]],
                 colnames(ttt)[i],
                 extraLevels = extraLevels,
                 includeNAs=includeNAs,
                 orderedAsUnordered=orderedAsUnordered,
                 factorlevellimit = factorlevellimit,
                 show.minmax = show.minmax,
                 show.IQR = show.IQR,
                 sd_digits = sd_digits,
                 descr_digits = descr_digits,
                 significant_digits = significant_digits)
    }
  } else if (format=="long") {
    tttt <- foreach(i=1:length(ttt), .combine=rbind) %do% {
      calc_descr_long(ttt[[i]],
                      colnames(ttt)[i],
                      extraLevels = extraLevels[[colnames(ttt)[i]]],
                      includeNAs=includeNAs,
                      orderedAsUnordered=orderedAsUnordered,
                      factorlevellimit = factorlevellimit,
                      show.minmax = show.minmax,
                      show.IQR = show.IQR,
                      sd_digits = sd_digits,
                      descr_digits = descr_digits,
                      significant_digits = significant_digits)
    }
  } else {
    stop(paste("unsupported format '",
               format,
               "'; allowed values are 'long' and 'wide'", sep=""))
  }

  colnames(tttt)[grep(".id", colnames(tttt), fixed=TRUE)] <- "parameter"

  ##tttt <-
  ##  rbind(data.frame(parameter="parameter",
  ##                   `mean \\pm sd`="statistics",
  ##                   `median (minimum, maximum)`="median(minimum; maximum)",
  ##                   NAs="NAs",
  ##                   stringsAsFactors=FALSE),
  ##        tttt)

  tttt
  ##tttt[apply(tttt, 1, function(x) !all((x[-1] == "") | is.na(x[-1]))),]
}


## the interface for the following function should be rewritten.
## I am thinking on sth along the lines of
## > buildDestrTbl(somedataframe,
##                 var1=w.ordered(var1,
##                     name="column header name",
##                     unit="cm",
##                     ignore_values=c("Unknown", "none"),
##                     test=list(w.chisq.test, simulate.p.value=TRUE),
##                     format=some_formatting_function),
##                 var2=w.survival(var_times, var_event,
##                     name="DFS",
##                     unit="years")
##                 var3=w.factor(...))


##' Calculate and Present Descriptive Values in Pritable
##'
##' Do a Table containing descriptiva values
##' @title buildDescrTbl
##' @param df data.frame containing the variables of which to calc the descriptive values
##' @param tests character vector or list of characters or list of functions or list of lists.  In each case the i-th element gives the test to perform on the ith variable in the df (excluding stratification variables).  The test can either be given as character (name of test function) or as function or as list where the first element is again either character or function and the following elemenst are *named* additional arguments to that test function.  The individual function has to accept (at least) the arguments 'values' and 'grouping' which are vectors of equal length.  For convenience, this package shipes with some example functions; have a look at those if you want to supply your own.  These convenience functions include w.chisq.test  w.cor.test, w.fisher.test,      w.kruskal.test, w.wilcox.test.  the whole list/vector is recycled if too short.
##' @param prmnames names of the variables in df (if needed to be overwritten)
##' @param prmunits units of the variables in df
##' @param addFactorLevelsToNames logical. if \code{TRUE} expand 'sex' to 'sex [m/w]'.  Defaults to \code{TRUE}.
##' @param excel_style logical. if TRUE remove subsequent duplicates from the \code{parameter} column (as common in Excel).  Default: \code{TRUE}
##' @param groupby column of df. do more columns - one for each group.  If the df$column is an ordered factor, the order will be respected in the resulting table
##' @param addungrouped logical.  if \code{TRUE} add a column 'total' with the ungrouped summary statistics.  Default: \code{FALSE}
##' @param dopvals boolean. if TRUE an additional column containing the p-values comparing the two strata in \code{groubpy}.  Only implemented for a two-level stratum until now.
##' @param ignore_test_errors logical. If \code{TRUE} returns an empty test results (as list).
##' @param p.adjust.method character.  if not NULL include an
##'   additional column with adjusted p values.  see
##'   \code{\link[stats]{p.adjust.methods}} for possible values and
##'   explanations.  Defaults to "holm"
##' @param orderedAsUnordered logical. treat ordered factors as unordered factors?
##' @param factorlevellimit integer.  for factors with more than
##'   \code{factorlevellimit} levels, not all levels are printed
##' @param show.minmax logical. if TRUE show minimum and maximum for numeric variables.  Defaults to \code{TRUE}.
##' @param show.IQR logical. if TRUE show 25\% and 75\% quantiles for numeric variables.  Defaults to \code{FALSE}.
##' @param report_tests boolean.  if TRUE one additional column in the result table will contain the test, that was performed to calculate the p value.  Ignored if dopvals=FALSE
##' @param report_testmessages boolean.  if TRUE one additional column in the result table will contain any warnings that appeared while the test was performed.  Ignored if dopvals=FALSE
##' @param pvals_formatting boolean.  If FALSE report numbers, else report formatted strings (via prettyPvalue)
##' @param pvals_digits integer.  Number of digits for p value formatting.  Ignored when pvals_formatting==FALSE.  Defaults to 2
##' @param pvals_signiflev double.  The significance level for bold p value formatting.  Ignored when pvals_formatting==FALSE.  Defaults to 0.05
##' @param format in c("wide", "long")
##' @param extraLevels named list of lists.  Names have to be variable names.  Elements have to have to be named list of this form: \code{`some label` = list(idxvec = idxvec, display = logical)}.  Here \code{idxvec} needs to be a logical vector of length \code{nrow(df)} that specifies the affected rows.  If \code{display} is \code{TRUE} the number of affected rows will be shown under \code{some label}.
##' @param missingName character.  name of missing values (default: \code{missing})
##' @param removeZeroNAs boolean.  if TRUE, rows for missing values containing only 0s are removed from the result.
##' @param removeZeroExtraLevels boolean.  if TRUE, rows for ExtraLevels containing only 0s are removed from the result.
##' @param includeNAs boolean.  Include number of NAs in the output?
##' @param printOrgAlignment boolean.  If TRUE, than a row like "<l>  <r>   <r>" will be included in the result df
##' @param useutf8 character.  one of c("latex", "utf8", "replace").
##'   if 'latex' (the default) use \\pm in the output;
##'   if 'replace' use +- in the output,
##'   if 'utf8' use the unicode character
##' @param verbose numeric.  level of verbosity (0 : silent)
##' @param without_attrs logical.  If \code{TRUE} return the descriptive table with attrs.  Otherwise add \code{df}, \code{groupby}, and a 'full' (closer to tidy) version of the table as attributes.  Defaults to \code{TRUE}.
##' @param sd_digits character.  one of c("by_mean", "fixed").  If 'by_mean', the number of decimal places of the standard deviation is limited by the number of decimal places of the mean.
##' @param descr_digits integer. Number of digits for formatting of descriptive values. Defaults to 2.
##' @param significant_digits boolean.  if TRUE, the number of significant digits of is given by descr_digits. Otherwise the number of decimal places is fixed.
##' @param percentages character.  one of c("columnwise", "rowwise").  If 'rowwise', percentages are computed by row.  Defaults to "columnwise"
##' @return formatted \code{data.frame} with descriptive values
##' @author Andreas Leha
##' @export
##' @examples
##' ttt <- data.frame(data="training set",
##'                   age=runif(100, 0, 100),
##'                   sex=sample(c("m","f"), 100, replace=TRUE, prob=c(0.3, 0.7)),
##'                   score=factor(sample(1:5, 100, replace=TRUE),
##'                     ordered=TRUE,
##'                     levels=1:5))
##' ttt2 <- data.frame(data="test set",
##'                    age=runif(100, 0, 100),
##'                    sex=sample(c("m","f"), 100, replace=TRUE, prob=c(0.5,0.5)),
##'                    score=factor(sample(1:5, 100, replace=TRUE),
##'                      ordered=TRUE,
##'                      levels=1:5))
##'
##' units <- c("years", "", "")
##' buildDescrTbl(rbind(ttt, ttt2),
##'               prmunits=units,
##'               groupby="data",
##'               includeNAs=TRUE)
buildDescrTbl <- function(df,
                          tests,
                          prmnames,
                          prmunits,
                          addFactorLevelsToNames = TRUE,
                          excel_style = TRUE,
                          groupby,
                          addungrouped = FALSE,
                          dopvals=FALSE,
                          ignore_test_errors = FALSE,
                          p.adjust.method = "holm",
                          orderedAsUnordered=FALSE,
                          factorlevellimit = 14,
                          show.minmax = TRUE,
                          show.IQR = FALSE,
                          report_tests=FALSE,
                          report_testmessages=FALSE,
                          pvals_formatting=TRUE,
                          pvals_digits=2,
                          pvals_signiflev=0.05,
                          format="long",
                          extraLevels = NULL,
                          missingName="missing",
                          removeZeroNAs=TRUE,
                          removeZeroExtraLevels = TRUE,
                          includeNAs=FALSE,
                          printOrgAlignment=FALSE,
                          useutf8 = "latex",
                          verbose = 0,
                          without_attrs = FALSE,
                          sd_digits = "by_mean",
                          descr_digits = 2,
                          significant_digits = TRUE,
                          percentages = "columnwise") {

  charactercols <- names(which(sapply(df, function(x) is.character(x) && !is.tod(x))))
  if (length(charactercols) > 0)
    warning("There are columns of type 'character' in the data.", "\n",
            "These will be ignored, but this might lead to problems anyway.", "\n",
            "Affected:", "\n  ",
            paste(charactercols, collapse = "\n  "))

  if (!without_attrs) {
    restbl_full <- buildDescrTbl(df                  = df,
                                 tests               = tests,
                                 ##prmnames            = prmnames,
                                 ##prmunits            = prmunits,
                                 addFactorLevelsToNames = FALSE,
                                 excel_style         = FALSE,
                                 groupby             = groupby,
                                 addungrouped        = addungrouped,
                                 dopvals             = dopvals,
                                 ignore_test_errors = ignore_test_errors,
                                 p.adjust.method     = p.adjust.method,
                                 orderedAsUnordered  = orderedAsUnordered,
                                 factorlevellimit    = factorlevellimit,
                                 show.minmax         = show.minmax,
                                 show.IQR            = show.IQR,
                                 report_tests        = TRUE,
                                 report_testmessages = TRUE,
                                 pvals_formatting    = FALSE,
                                 pvals_digits        = pvals_digits,
                                 pvals_signiflev     = pvals_signiflev,
                                 format              = "long",
                                 extraLevels         = extraLevels,
                                 missingName         = missingName,
                                 removeZeroNAs       = removeZeroNAs,
                                 removeZeroExtraLevels = removeZeroExtraLevels,
                                 includeNAs          = includeNAs,
                                 printOrgAlignment   = printOrgAlignment,
                                 useutf8             = useutf8,
                                 verbose             = 0,
                                 without_attrs       = TRUE,
                                 sd_digits           = sd_digits,
                                 descr_digits        = descr_digits,
                                 significant_digits  = significant_digits,
                                 percentages = "columnwise")
  }

  if (missing(groupby)) {
    restbl <- buildDescrTbl.intern(df,
                                   prmnames,
                                   prmunits,
                                   addFactorLevelsToNames = addFactorLevelsToNames,
                                   format=format,
                                   extraLevels = extraLevels,
                                   includeNAs=includeNAs,
                                   orderedAsUnordered=orderedAsUnordered,
                                   factorlevellimit = factorlevellimit,
                                   show.minmax = show.minmax,
                                   show.IQR = show.IQR,
                                   sd_digits = sd_digits,
                                   descr_digits = descr_digits,
                                   significant_digits = significant_digits)
  } else {
    desctbls <- list()
    groupfactor <- droplevels(as.factor(df[[groupby]]))
    for (i in 1:length(levels(groupfactor))) {
      group <- levels(groupfactor)[i]
      tdf <- df[!is.na(df[[groupby]]) & df[[groupby]] == group,-match(groupby, colnames(df)), drop = FALSE]
      textraLevels <- lapply(extraLevels, function(x) lapply(x, function(xi) {xi$idxvec <- xi$idxvec[!is.na(df[[groupby]]) & df[[groupby]] == group]; xi}))
      desctbls[[i]] <- buildDescrTbl.intern(tdf,
                                            prmnames,
                                            prmunits,
                                            addFactorLevelsToNames = addFactorLevelsToNames,
                                            format=format,
                                            extraLevels = textraLevels,
                                            includeNAs=includeNAs,
                                            orderedAsUnordered=orderedAsUnordered,
                                            factorlevellimit = factorlevellimit,
                                            show.minmax = show.minmax,
                                            show.IQR = show.IQR,
                                            sd_digits = sd_digits,
                                            descr_digits = descr_digits,
                                            significant_digits = significant_digits)
    }
    names(desctbls) <- levels(groupfactor)
    ##tbl <- cbind(parameter=desctbls[[1]][,1],
    ##             do.call(cbind, lapply(desctbls, function(tab) tab[,-1])))

    ## rename the value columns
    for (i in 1:length(desctbls)) {
      colnames(desctbls[[i]]) <- gsub("^value$", names(desctbls)[i], colnames(desctbls[[i]]))
    }

    ## merge
    ## we use plyr::join() here to avoid re-orderings
    tbl <- Reduce(function(x,y) plyr::join(x,y, by=c("colname", "parameter", "level"), type="full"), desctbls)

    if (dopvals) {
      grouplevels <- levels(groupfactor)

      need_tests_for <- unique(intersect(tbl$colname, colnames(df)))

      suggestTest <- function(x, nlevels)
      {
        if (is.factor(x)) {
          if (is.ordered(x)) {
            if (nlevels == 2) {
              "w.wilcox.test"
            } else {
              "w.kruskal.test"
            }
          } else {
            "w.fisher.test"
          }
        } else if (inherits(x, "Date") || inherits(x, "POSIXt")) {
          warning("no test suggestion for variable of type '", class(x)[1], "'")
          "w.no.test"
        } else if (is.tod(x)) {
          "w.watson.williams.test"
        } else if (is.numeric(x)) {
          if (nlevels == 2) {
            "w.t.test"
          } else {
            "w.anova.test"
          }
        } else {
          warning("no test suggestion for variable of type '", class(x)[1], "'")
          "w.no.test"
        }
      }

      if (!missing(tests) && is.null(names(tests)))
        stop("'tests' needs to be named")

      if (missing(tests) ||                            ## no tests specified
          !all(need_tests_for %in% names(tests))) {    ## not all tests specified
        suggested_tests <- sapply(need_tests_for, function(var) suggestTest(df[[var]], length(grouplevels)))
      } else {
        suggested_tests <- sapply(need_tests_for, function(var) "w.no.test")
      }
      names(suggested_tests) <- need_tests_for

      usetests <- suggested_tests

      if (!missing(tests)) {
        supplied_tests_for <- intersect(need_tests_for, names(tests))
        if (!all(names(tests) %in% need_tests_for))
          warning(paste(names(tests)[!names(tests) %in% need_tests_for], collapse = ", "),
                  " not in the descriptive table")
        usetests[supplied_tests_for] <- tests[supplied_tests_for]
      }

      tbl$`p value`   <- NA
      tbl$`adjusted p value` <- NA
      tbl$test        <- ""
      tbl$testmessage <- ""

      for (var in unique(intersect(tbl$colname, colnames(df)))) {
        if (verbose) print(var)
        varheadrow <- min(which(tbl$colname == var))

      ##seprows <- grep("\\vert", gsub(" \\[.*\\]$", "", tbl[,1]), invert=T)
      ##pvals <- rep(NA, nrow(tbl))
      ##pvalmethods <- rep("", nrow(tbl))
      ##pvalmessages <- rep("", nrow(tbl))
      ##for (var in colnames(df)[-match(groupby, colnames(df))]) {
      ##  print(var)
      ##  varheadrow <- seprows[which(gsub(" \\[.*\\]$", "", tbl[seprows,1]) == var)]
        ##if (!missing(tests)) {
          testres <- do.call("testWrapper",
                             c(usetests[[var]],
                               list(values=df[[var]],
                                    grouping=df[[groupby]],
                                    ignore_test_errors = ignore_test_errors)))
          tbl$`p value`[varheadrow] <- testres$p.value
          if (!is.null(testres$method))
              tbl$test[varheadrow] <- testres$method
          if (!is.null(testres$warnings))
              tbl$testmessage[varheadrow] <- testres$warnings
        ##} else {
        ##  tbl$`p value`[varheadrow] <- if(is.factor(df[,var]) && is.ordered(df[,var])) {
        ##    cor.test(as.numeric(df[,groupby]),
        ##             as.numeric(df[,var]), method="kendall")$p.value
        ##  } else if(is.factor(df[,var]) && nlevels(df[,var]) == 2 && length(unique(df[,groupby])) == 2) {
        ##    fisher.test(table(df[,groupby], df[,var]))$p.value
        ##  } else if(is.factor(df[,var])) {
        ##    chisq.test(table(df[,groupby], df[,var]))$p.value
        ##  } else {
        ##    wilcox.test(df[df[[groupby]] == grouplevels[1],var],
        ##                df[df[[groupby]] == grouplevels[2],var])$p.value
        ##  }
        ##}
      }
      if (!is.null(p.adjust.method)) {
        tbl$`adjusted p value` <- p.adjust(tbl$`p value`, method = p.adjust.method)
      } else {
        tbl <- tbl[,colnames(tbl) != "adjusted p value", drop = FALSE]
      }
      if (pvals_formatting) {
        tbl$`p value` <- prettyPvalues(tbl$`p value`, digits=pvals_digits, signiflev = pvals_signiflev)
        if (!is.null(p.adjust.method))
          tbl$`adjusted p value` <- prettyPvalues(tbl$`adjusted p value`, digits=pvals_digits, signiflev = pvals_signiflev)
      }
      tbl$`p value`[grep("NA", tbl$`p value`)] <- ""
      tbl$`p value`[is.na(tbl$`p value`)] <- ""
      if (!is.null(p.adjust.method)) {
        tbl$`adjusted p value`[grep("NA", tbl$`adjusted p value`)] <- ""
        tbl$`adjusted p value`[is.na(tbl$`adjusted p value`)] <- ""
      }
      ##tbl <- cbind(tbl, `p value`=pvals)
      if (!report_tests)
          tbl <- tbl[,colnames(tbl) != "test", drop = FALSE]
      if (!report_testmessages)
          tbl <- tbl[,colnames(tbl) != "testmessage", drop = FALSE]
    }
    if (printOrgAlignment) {
      orgAlignment <- t(c("<l>", rep("<r>", ncol(tbl)-1)))
      colnames(orgAlignment) <- colnames(tbl)
      tbl <- rbind(orgAlignment, tbl)
    }
    restbl <- tbl
  }

  ## remove the 'colname' column
  restbl <- restbl[,colnames(restbl) != "colname"]

  ## add ungrouped
  if (addungrouped | (percentages == "rowwise" & !missing(groupby))) {
    restbl_u <-
      buildDescrTbl(
        df = df[, !colnames(df) %in% groupby],
        tests = tests,
        prmnames = prmnames,
        prmunits = prmunits,
        addFactorLevelsToNames = addFactorLevelsToNames,
        excel_style = FALSE,
        ##groupby,
        addungrouped = FALSE,
        dopvals = FALSE,
        ignore_test_errors = FALSE,
        ##p.adjust.method = "holm",
        orderedAsUnordered = orderedAsUnordered,
        factorlevellimit = factorlevellimit,
        show.minmax = show.minmax,
        show.IQR = show.IQR,
        ##report_tests=FALSE,
        ##report_testmessages=FALSE,
        ##pvals_formatting=TRUE,
        ##pvals_digits=2,
        ##pvals_signiflev=0.05,
        format = format,
        extraLevels         = extraLevels,
        missingName = "\\\\vert missing",
        removeZeroNAs = FALSE,
        removeZeroExtraLevels = FALSE,
        includeNAs = includeNAs,
        printOrgAlignment = printOrgAlignment,
        useutf8 = "latex",
        ##useutf8 = useutf8,
        verbose = verbose,
        without_attrs = TRUE,
        sd_digits = "by_mean",
        descr_digits = descr_digits,
        significant_digits = significant_digits)
    restbl <- full_join(dplyr::rename(restbl_u, total = .data$value), restbl)
  }

  ## 'excel-style' headings
  if (excel_style) {
    restbl <- convertColumnHeading(restbl, "parameter", movealong = c("p value", "adjusted p value", "test", "testmessage"))
  }

  ## remove 'missing' rows if no missing values are reported
  if (removeZeroNAs) {
    NAlines <- grep("^\\\\vert missing", restbl$level)
    remlines <-
      sapply(NAlines, function(linum)
      {
        NAline <- restbl[linum,!colnames(restbl) %in% c("parameter", "level", "p value", "adjusted p value", "test", "testmessage")]
        NAvals <- as.numeric(gsub(" \\(.*%\\)", "", NAline))
        if (sum(NAvals) == 0) {
          linum
        } else {
          NA
        }
      })
    if (sum(!is.na(remlines)) > 0)
      restbl <- restbl[-na.omit(remlines),]
  }

  ## remove extraLevel rows if no cases on the extraLevel are reported
  if (removeZeroExtraLevels && !is.null(extraLevels)) {
    extraLevelLabels <- unique(unlist(lapply(extraLevels, names)))
    ExtraLevelLines <- which(restbl$level %in% extraLevelLabels)
    remlines <-
      sapply(ExtraLevelLines, function(linum)
      {
        ExtraLevelLine <- restbl[linum,!colnames(restbl) %in% c("parameter", "level", "p value", "adjusted p value", "test", "testmessage")]
        ExtraLevelVals <- as.numeric(gsub(" \\(.*%\\)", "", ExtraLevelLine))
        if (sum(ExtraLevelVals) == 0) {
          linum
        } else {
          NA
        }
      })
    if (sum(!is.na(remlines)) > 0)
      restbl <- restbl[-na.omit(remlines),]
  }

  ## use the given 'missingName'
  restbl$level <- gsub("^\\\\vert missing$", missingName, restbl$level)
  if (useutf8 == "utf8") {
    for (i in 1:ncol(restbl)) restbl[[i]] <- gsub("\\\\pm", "\u00B1", restbl[[i]])
  } else if (useutf8 == "replace") {
    for (i in 1:ncol(restbl)) restbl[[i]] <- gsub("\\\\pm", "+-", restbl[[i]])
  }

  ## strip rownames
  rownames(restbl) <- NULL


  ## add some passthrough information in the attributes
  if (!without_attrs) {
    attr(restbl, "descrtbl_full") <- restbl_full
    attr(restbl, "data")          <- df
    if (!missing(groupby)) {
      attr(restbl, "groupby")       <- groupby
    } else {
      attr(restbl, "groupby")       <- NULL
    }
  }

  if (percentages == "rowwise" & !missing(groupby))
    restbl <- restbl %>% mutate_at(names(restbl %>%
                                         select_if(!colnames(.) %in% c("parameter", "level", "total", "p value", "adjusted p value", "test", "testmessage"))),
                                   ~ if_else(grepl("%", total, fixed = TRUE)  | parameter == "n",
                                             paste0(stringr::word(., 1), " (", gsub("[[:blank:]]", "", format(round(as.numeric(stringr::word(., 1))/as.numeric((stringr::word(total, 1)))*100, digits = 1), nsmall=1)), "%)"), .)) %>%
                         mutate(total = if_else(grepl("%", total, fixed = TRUE)  | parameter == "n",
                                                paste(stringr::word(total, 1), "(100%)"), total))

  return(restbl)
}

##' wrap this around 'correlation' tests to get output formatted for buildDescrTbl
##'
##' This function is called by buildDescrTbl in order to generate the
##' comparison p values.  Basically it just calls the provided testfun.
##' Main purpose is, that it collects warnings and returns them as well.
##' @title Collect Warnings From Runnning testfun
##' @param testfun character or function.  Which function to call.
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ignore_test_errors logical. If \code{TRUE} returns an empty test results (as list).
##' @param ... additional parameters.  are passed on to the testfun
##' @return list.  the results from testfun plus the element 'warnings'
##'         containing all warnings collected from the run of testfun.
##'         the results from testfun are assumed to be of type list and
##'         are additionally assumed to contain at least the elements 'p.value'
##'         and 'method'.
##' @author Andreas Leha
testWrapper <- function(testfun, values, grouping, ignore_test_errors = FALSE, ...) {
  warning_separator <- ";"
  local_warnings <- NULL
  testfun_robustified <- purrr::possibly(.f=testfun,
                                         otherwise = list(statistic = NA,
                                                          parameter = NULL,
                                                          p.value = NA,
                                                          null.value = NULL,
                                                          alternative = NULL,
                                                          method = "<failed>",
                                                          data.name = NULL))
  testfun_to_call <- if (ignore_test_errors) testfun_robustified else testfun
  test_result <-
    withCallingHandlers(do.call(testfun_to_call,
                                c(list(values=values,
                                       grouping=grouping),
                                  list(...))),
                          warning=function(w)
                        {
                          local_warnings <<- paste(local_warnings,
                                                   w$message,
                                                   sep=warning_separator)
                          invokeRestart( "muffleWarning" )
                        })
  local_warnings <- gsub(paste0("^", warning_separator), "", local_warnings)

  c(test_result, warnings=local_warnings)
}

##' wilcox.test with unified interface
##'
##' just a call to wilcox.test that unifies the api to other tests
##' @title wilcox.test with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the wilcox.test
##' @return the value wilcox.test
##' @export
##' @author Andreas Leha
w.wilcox.test <- function(values, grouping, ...) {
  values <- as.numeric(values)
  gvalues <- splitGrps(values, grouping)
  if (length(gvalues) != 2) stop("w.wilcox.test is applicable with *two* groups only")
  failwith(default = list(statistic = NA,
                          parameter = NULL,
                          p.value = NA,
                          null.value = NULL,
                          alternative = NULL,
                          method = NULL,
                          data.name = NULL),
           f = wilcox.test)(as.numeric(gvalues[[1]]),
              as.numeric(gvalues[[2]]),
              ...)
}

##' npar.t.test with unified interface
##'
##' just a call to npar.t.test that unifies the api to other tests
##' computes the ordinary Brunner-Munzel test for group sizes > 9 and the studentized permutation test version otherwise
##' @title npar.t.test with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the npar.t.test
##' @return the value npar.t.test
##' @export
##' @author Fabian Kück

w.npar.t.test <- function(values, grouping, ...) {
  values <- as.numeric(values)
  glevels <- unique(grouping)
  if (length(glevels) != 2) stop("w.nparcomp.test is applicable with *two* groups only")
  df <- data.frame(group=grouping, value=values)

  if(!(sum(!is.na(values[grouping==unique(grouping)[1]]), na.rm = TRUE) < 10 | sum(!is.na(values[grouping==unique(grouping)[2]]), na.rm = TRUE) < 10))
    method <- "t.app"
  else if(!(sum(!is.na(unique(values[grouping==unique(grouping)[1]])), na.rm = TRUE) < 7 | sum(!is.na(unique(values[grouping==unique(grouping)[2]])), na.rm = TRUE) < 7))
    method <- "permu"
  else
  {
    method <- "permu"
    warning("Too few observations in at least one group. Approximation may be incorrect.")
  }

  npar.t.test_wrapper <- function(method="t.app", ...) {
    res <- nparcomp::npar.t.test(method=method, ...)
    res$methodvec  <- if_else(method=="t.app", "Brunner Munzel test", "Studentized permutation test")
    if(method=="t.app")
      res$p.value <- res$Analysis$p.Value else
      res$p.value <- res$Analysis$p.value[1]
    return(res)
  }
  plyr::failwith(default = list(statistic = NA,
                                parameter = NULL,
                                p.value = NA,
                                null.value = NULL,
                                alternative = NULL,
                                method = NULL,
                                data.name = NULL),
                 f = npar.t.test_wrapper)(value ~ group,
                                          data = df, alternative = "two.sided", info = FALSE, method = method,
                                          ...)
}

##' npar.t.test with unified interface
##'
##' just a call to npar.t.test that unifies the api to other tests
##' computes the the studentized permutation test version of the Brunner-Munzel test
##' @title npar.t.test with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the npar.t.test
##' @return the value npar.t.test
##' @export
##' @author Fabian Kück

w.npar.t.test.permu <- function(values, grouping, ...) {
  values <- as.numeric(values)
  glevels <- unique(grouping)
  if (length(glevels) != 2) 
    stop("w.nparcomp.test is applicable with *two* groups only")
  df <- data.frame(group = grouping, value = values)
  if (!(sum(!is.na(unique(values[grouping == unique(grouping)[1]])), 
            na.rm = TRUE) < 7 | sum(!is.na(unique(values[grouping == unique(grouping)[2]])), na.rm = TRUE) < 7)) 
    method <- "permu"
  else {
    method <- "permu"
    warning("Too few observations in at least one group. Approximation may be incorrect.")
  }
  npar.t.test_wrapper <- function(method="t.app", ...) {
    res <- nparcomp::npar.t.test(method=method, ...)
    res$methodvec  <- if_else(method=="t.app", "Brunner Munzel test", "Studentized permutation test")
    if(method=="t.app")
      res$p.value <- res$Analysis$p.Value else
      res$p.value <- res$Analysis$p.value[1]
    return(res)
  }
  plyr::failwith(default = list(statistic = NA,
                                parameter = NULL,
                                p.value = NA,
                                null.value = NULL,
                                alternative = NULL,
                                method = NULL,
                                data.name = NULL),
                 f = npar.t.test_wrapper)(value ~ group,
                                          data = df, alternative = "two.sided", info = FALSE, method = method,
                                          ...)
}

##' t.test with unified interface
##'
##' just a call to t.test that unifies the api to other tests
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the t.test
##' @return the value of t.test
##' @export
##' @author Andreas Leha
w.t.test <- function(values, grouping, ...) {
  glevels <- unique(grouping)
  if (length(glevels) != 2) stop("w.t.test is applicable with *two* groups only", "\n",
                         "got ", length(glevels))
  failwith(default = list(statistic = NA,
                          parameter = NULL,
                          p.value = NA,
                          null.value = NULL,
                          alternative = NULL,
                          method = NULL,
                          data.name = NULL),
           f = t.test)(values[grouping == glevels[1]],
             values[grouping == glevels[2]],
             ...)
}

##' ANOVA with unified interface
##'
##' One-way ANOVA that unifies the api to other tests
##' @param values vector.  The values to compare (age, gene
##'   expression, ...)
##' @param grouping vector ef the same length as \code{values}.
##'   treated as factor giving the group membership
##' @param na.rm logcical.  if TRUE (default) the \code{values} are subset to only non-missing and finite values
##' @param ... additional parameters.  are passed on to \code{lm()}
##' @return the value of \code{anova} augmented by 'p.value' and 'method'
##' @export
##' @author Dr. Andreas Leha
w.anova.test <- function(values, grouping, na.rm = TRUE, ...) {
  if (na.rm) {
    idx <- (is.na(values)) | (!is.finite(values))
    values <- values[!idx]
    grouping <- grouping[!idx]
  }
  glevels <- unique(grouping)
  lmanova <- function(values, grouping, ...) {
    anova(lm(values ~ grouping, ...))
  }
  lmanovares <-
    failwith(default = list(DF        = c(NA, NA),
                            `Sum Sq`  = c(NA, NA),
                            `Mean Sq` = c(NA, NA),
                            `F value` = c(NA, NA),
                            `Pr(>F)`  = c(NA, NA)),
             f = lmanova)(values, grouping,
               ...)
  ##names(res) <- gsub("Pr\\(>F\\)", "p.value", names(res))
  res <- list(lmanovares = lmanovares)
  res$p.value = lmanovares$`Pr(>F)`[1]
  res$method = "Analysis of Variance"
  res
}

##' kruskal.test with unified interface
##'
##' just a call to kruskal.test that unifies the api to other tests
##' @title kruskal.test with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the kruskal.test
##' @return the value kruskal.test
##' @export
##' @author Andreas Leha
w.kruskal.test <- function(values, grouping, ...) {
  kruskal.test(as.numeric(values),
               as.factor(grouping),
               ...)
}

##' JonckheereTerpstraTest with unified interface
##'
##' just a call to JonckheereTerpstraTest that unifies the api to other tests
##' @title JonckheereTerpstraTest with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the JonckheereTerpstraTest
##' @return the value JonckheereTerpstraTest
##' @export
##' @author Fabian Kück
w.JonckheereTerpstraTest <- function(values, grouping, ...) {
  JonckheereTerpstraTest(x = as.numeric(values), g = as.factor(grouping), ...)
}

##' rankFD with unified interface
##'
##' just a call to rankFD that unifies the api to other tests
##' @title rankFD with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the rankFD
##' @return the value rankFD
##' @export
##' @author Fabian Kück
w.rankFD.pseudo.ranks <- function (values, grouping, ...)
{
  rankFD_wrapper <- function(...) {
    res <- rankFD(...)
    res$p.value <- res$ANOVA.Type.Statistic[4]
    res$method <- "Pseudo-rank based test for independent groups"
    return(res)
  }
  df <- data.frame(values=as.numeric(values), grouping=as.factor(grouping))
  plyr::failwith(default = list(statistic = NA, parameter = NULL,
                                p.value = NA, null.value = NULL, alternative = NULL,
                                method = "Pseudo-rank based test for independent groups", data.name = NULL), f = rankFD_wrapper)(values~grouping, hypothesis ="H0p", sci.method ="fisher", data=df, ...)
}

##' rankFD with unified interface
##'
##' just a call to rankFD with effect="weighted" that unifies the api to other tests
##' @title rankFD with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the rankFD
##' @return the value rankFD
##' @export
##' @author Fabian Kück

w.rankFD.mid.ranks <- function (values, grouping, ...)
{
  rankFD_wrapper <- function(...) {
    res <- rankFD(...)
    res$p.value <- res$ANOVA.Type.Statistic[4]
    res$method <- "Rank based test for independent groups"
    return(res)
  }
  df <- data.frame(values=as.numeric(values), grouping=as.factor(grouping))
  plyr::failwith(default = list(statistic = NA, parameter = NULL,
                                p.value = NA, null.value = NULL, alternative = NULL,
                                method = "Rank based test for independent groups", data.name = NULL), f = rankFD_wrapper)(values~grouping, hypothesis ="H0p", sci.method ="fisher", effect="weighted", data=df, ...)
}

##' fisher.test with unified interface
##'
##' just a call to fisher.test that unifies the api to other tests
##' computes the exact p-value if possible and simulates the p-value otherwise
##' @title fisher.test with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the fisher.test
##' @return the value fisher.test
##' @export
##' @author Andreas Leha
w.fisher.test <- function(values, grouping, ...) {
  res <- tryCatch(
    fisher.test(table(values, grouping), ...),
    error = function(e) NULL)
  if(is.null(res))
    res <- fisher.test(table(values, grouping), simulate.p.value=TRUE, B=100000,  ...)
  return(res)
}


##' no test but unified interface
##'
##' just returns NA.  Included as pass through for non-testable variables
##' @title no test
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  unused
##' @return NA
##' @export
##' @author Andreas Leha
w.no.test <- function(values, grouping, ...) {
  res <- list(
    p.value  = NA,
    method   = "<untested>",
    warnings = "")
  return(res)
}


##' cor.test with unified interface
##'
##' just a call to cor.test that unifies the api to other tests
##' @title cor.test with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the cor.test
##' @return the value cor.test
##' @export
##' @author Andreas Leha
w.cor.test <- function(values, grouping, ...) {
  cor.test(as.numeric(values),
           as.numeric(grouping),
           ...)
}

robustdroplevels <- function(x)
{
  if (is.factor(x)) droplevels(x) else x
}

##' chisq.test with unified interface
##'
##' just a call to chisq.test that unifies the api to other tests
##' @title chisq.test with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the chisq.test
##' @return the value chisq.test
##' @export
##' @author Andreas Leha
w.chisq.test <- function(values, grouping, ...) {
  x <- table(robustdroplevels(values), robustdroplevels(grouping))
  y <- apply(x, 2, function(v) sum(v == 0) / length(v))
  idx <- which(y == 1)
  if (length(idx) > 0) {
    warning(paste("dropping", paste(names(idx), collapse = ", "), "as there are no observations"))
    x <- x[,-idx]
  }
  chisq.test(x, ...)
}

##' prop.trend.test with unified interface
##'
##' just a call to prop.trend.test that unifies the api to other tests
##' @title prop.trend.test with unified interface
##' @param values vector.  The values to compare (age, toxicity score, gene expression, ...)
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the prop.trend.test
##' @return the value prop.trend.test
##' @export
##' @author Fabian Kück
w.prop.trend.test <- function(values, grouping, ...) {
  if(length(unique(values)) != 2) stop("w.prop.trend.test is applicable for binary variables only")
  prop.trend.test(x = as.numeric(table(values, grouping)[1,]), n = as.numeric(table(values, grouping)[1,]) + as.numeric(table(values, grouping)[2,]), ...)
}

##' Watson-Williams Test of Homogeneity of Means
##'
##' just a call to \code{\link[circular]{watson.williams.test}} that unifies the api to other tests
##' @param values vector.  The values to compare.  Will be passed to \code{\link[circular]{circular}} to be converted to circular format
##' @param grouping vector of the same length as values.  treated as factor giving the
##'                 group membership
##' @param ... additional parameters.  are passed on to the \code{\link[circular]{watson.williams.test}}
##' @return the value of \code{\link[circular]{watson.williams.test}}
##' @export
##' @author Dr. Andreas Leha
w.watson.williams.test <- function(values, grouping, ...) {
  circular::watson.williams.test(circular(values), group = grouping, ...)
}

##' Cochran-Armitage Test for Trend
##'
##' Wrapper for \code{\link[DescTools]{CochranArmitageTest}} which is
##' a test for trend in binomial proportions across the levels of a
##' single variable
##' @param values vector.  The values to compare.  Will be passed to
##'     \code{\link[circular]{circular}} to be converted to circular
##'     format
##' @param groupingvector of the same length as values.  treated as
##'     factor giving the group membership
##' @param ... additional parameters.  are passed on to the
##'     \code{\link[DescTools]{CochranArmitageTest}}
##' @return the value of \code{\link[DescTools]{CochranArmitageTest}}
##' @export
##' @author Dr. Andreas Leha
w.CochraneArmitageTrend.test <- function(values, grouping, ...)
{
  if (length(na.omit(unique(values))) != 2) stop("Cochrane-Armitage test for trend can be applied to binary values only")
  x <- table(robustdroplevels(values), robustdroplevels(grouping))
  y <- apply(x, 2, function(v) sum(v == 0) / length(v))
  idx <- which(y == 1)
  if (length(idx) > 0) {
    warning(paste("dropping", paste(names(idx), collapse = ", "), "as there are no observations"))
    x <- x[,-idx]
  }
  DescTools::CochranArmitageTest(x, ...)
}

##' Internal wrapper around calc_descr_matrix
##'
##' @title buildDescrTbl.intern
##' @param df data.frame containing the variables of which to calc the descriptive values
##' @param prmnames names of the variables in df (if needed to be overwritten)
##' @param prmunits units of the variables in df
##' @param addFactorLevelsToNames logical. if \code{TRUE} expand 'sex' to 'sex [m/w]'.  Defaults to \code{TRUE}.
##' @param format in c("wide", "long")
##' @param extraLevels  named list of lists.  Names have to be variable names.  Elements have to have to be named list of this form: \code{`some label` = list(idxvec = idxvec, display = logical)}.  Here \code{idxvec} needs to be a logical vector of length \code{nrow(df)} that specifies the affected rows.  If \code{display} is \code{TRUE} the number of affected rows will be shown under \code{some label}.
##' @param includeNAs boolean.  Include number of NAs in the output?
##' @param orderedAsUnordered logical. treat ordered factors as
##'   unordered factors?
##' @param factorlevellimit integer.  for factors with more than
##'   \code{factorlevellimit} levels, not all levels are printed
##' @param show.minmax logical. if TRUE show minimum and maximum for numeric variables.  Defaults to \code{TRUE}.
##' @param show.IQR logical. if TRUE show 25\% and 75\% quantiles for numeric variables.  Defaults to \code{FALSE}.
##' @param sd_digits character.  one of c("by_mean", "fixed").  If 'by_mean', the number of decimal places of the standard deviation is limited by the number of decimal places of the mean.
##' @param descr_digits integer. Number of digits for formatting of descriptive values. Defaults to 2.
##' @param significant_digits boolean.  if TRUE, the number of significant digits of is given by descr_digits. Otherwise the number of decimal places is fixed.
##' @return matrix with descriptive values
##' @author Andreas Leha
buildDescrTbl.intern <- function(df,
                                 prmnames,
                                 prmunits,
                                 addFactorLevelsToNames = TRUE,
                                 format="long",
                                 extraLevels = NULL,
                                 includeNAs=FALSE,
                                 orderedAsUnordered=FALSE,
                                 factorlevellimit = 14,
                                 show.minmax = TRUE,
                                 show.IQR = FALSE,
                                 sd_digits = "by_mean",
                                 descr_digits = 2,
                                 significant_digits = TRUE) {
  if (missing(prmnames)) prmnames <- names(df)
  if (missing(prmunits)) prmunits <- rep("", length(df))
  names(prmnames) <- names(df)
  names(prmunits) <- names(df)

  tttt <- calc_descr_matrix(df,
                            format=format,
                            extraLevels = extraLevels,
                            includeNAs=includeNAs,
                            orderedAsUnordered=orderedAsUnordered,
                            factorlevellimit = factorlevellimit,
                            show.minmax = show.minmax,
                            show.IQR = show.IQR,
                            sd_digits = sd_digits,
                            descr_digits = descr_digits,
                            significant_digits = significant_digits)

  ## add a row with the number of samples at the top
  n_row <- tttt[1,]
  n_row[1,"parameter"] <- "n"
  n_row[1,"value"] <- nrow(df)
  n_row[1,"level"] <- ""
  n_row[is.na(n_row)] <- ""
  ##if (ncol(n_row) > 2) n_row[3:ncol(n_row)] <- ""
  tttt <- rbind(n_row,
                tttt)

  ## save the original names
  tttt$colname <- tttt$parameter

  ## adapt the names
  ## 1. use user supplied names (if given)
  ## 2. add the levels of factors to the name
  ## (but do not change 'n')
  if (nrow(tttt) > 1) {
    tttt$parameter[-1] <-
      sapply(tttt$parameter[-1], function(n) {
        ##if (length(grep("^\\\\vert", n)) == 0) {
        paste(prmnames[n],
              ifelse(!any(is.na(suppressWarnings(as.numeric(levels(df[[n]]))))),
                     ifelse(is.factor(df[[n]]) && addFactorLevelsToNames,
                            ifelse(length(levels(df[[n]]))>5,
                                   paste(" [", levels(df[[n]])[1], "/.../", rev(levels(df[[n]]))[1], "]", sep=""),
                                   paste(" [", paste(levels(df[[n]]), collapse="/"), "]", sep="")),
                            ifelse(prmunits[n] != "",
                                   paste(" [", prmunits[n], "]", sep=""),
                                   "")),
                     ""),
              sep="")
        ##} else {
        ##  n
        ##}
      })
  }

  tttt
}



##' X-years prediction
##'
##' Calculate the X-years estimate of a survfit
##' @title pred.survfit
##' @param Sfit a survfit object
##' @param time the time to calculate the estimate
##' @return the estimate
##' @author Andreas Leha
##' @export
pred.survfit <- function(Sfit, time) {
  low <- max(Sfit$time[Sfit$time <= time])
  high <- min(Sfit$time[Sfit$time >= time])

  C <- (time-low)/(high-low)

  lowi <- which(Sfit$time == low)
  highi <- which(Sfit$time == high)

  lowp <- Sfit$surv[lowi]
  highp <- Sfit$surv[highi]

  pred <- lowp - C*(lowp - highp)
  pred
}

##' build a description table for survival estimates
##'
##' calculate the survival estimates at the specified times and return a
##' nicely formatted table
##' @title descrSurvEstimate
##' @param S survival objects from \code{\link[survival]{Surv}}
##' @param strata a list of vectors containing strata.  If the vectors are ordered factors the columns will be used in the given order.
##' @param stratorder (list of) character vector for the order of the reported columns.  Overrides any order of strata
##' @param survname the name of the survival time, e.g. 'DFS'
##' @param evaltimes numeric vector.  for which times to calculate the survival estimate
##' @param evaltimeunits the unit of the survival times (years, months, ...)
##' @param digits round to
##' @param includeNAs boolean.  Include number of NAs in the output?
##' @param missingName character.  name of the rows with missing numbers.  Defaults to "missing".
##' @param stratheader boolean.  print the stratheader?  Turn off for inclusion into a bigger table
##' @param pval boolean.  if TRUE, the p value from a cox model is printed in a separate column
##' @param pvals_formatting boolean.  If FALSE report numbers, else report formatted strings (via prettyPvalue)
##' @param pvals_digits integer.  Number of digits for p value formatting.  Ignored when pvals_formatting==FALSE.  Defaults to 2
##' @param pvals_signiflev double.  The significance level for bold p value formatting.  Ignored when pvals_formatting==FALSE.  Defaults to 0.05
##' @param hr boolean.  if TRUE, the hazard ratio (with confidence interval) is printed as well.  (only has an effect if pval==TRUE)
##' @return a character matrix
##' @author Andreas Leha
##' @export
##' @examples
##' if (require("survival")) {
##'
##'   S <- Surv(aml$time, aml$status)
##'
##'   descrSurvEstimate(S,
##'                     evaltimes=c(19, 24),
##'                     evaltimeunits="months")
##' }
descrSurvEstimate <- function(S,
                              strata,
                              stratorder,
                              survname="survival time",
                              evaltimes=c(3,5),
                              evaltimeunits="years",
                              digits=2,
                              includeNAs=TRUE,
                              missingName="missing",
                              stratheader=TRUE,
                              pval=FALSE,
                              pvals_formatting=TRUE,
                              pvals_digits=2,
                              pvals_signiflev=0.05,
                              hr=FALSE) {

  if (missing(strata))
    return(descrSurvEstimate.single(S,
                                    survname=survname,
                                    evaltimes=evaltimes,
                                    evaltimeunits=evaltimeunits,
                                    includeNAs=includeNAs,
                                    missingName=missingName,
                                    digits=digits))

  res <- descrSurvEstimate.intern(S=S,
                                  strata=strata,
                                  stratorder=stratorder,
                                  idx=1:nrow(S),
                                  survname=survname,
                                  evaltimes=evaltimes,
                                  evaltimeunits=evaltimeunits,
                                  digits=digits,
                                  includeNAs=includeNAs,
                                  missingName=missingName,
                                  stratheader=stratheader,
                                  pval=pval,
                                  pvals_formatting=pvals_formatting,
                                  pvals_digits=pvals_digits,
                                  pvals_signiflev=pvals_signiflev,
                                  hr=hr)

  rownames(res) <- NULL
  return(res[,-grep(survname, res[1,])[-1]])
}


descrSurvEstimate.intern <- function(S,
                                     strata,
                                     stratorder,
                                     idx,
                                     survname="survival time",
                                     evaltimes=c(3,5),
                                     evaltimeunits="years",
                                     digits=2,
                                     includeNAs=TRUE,
                                     missingName="missing",
                                     stratheader=TRUE,
                                     pval=FALSE,
                                     pvals_formatting=TRUE,
                                     pvals_digits=2,
                                     pvals_signiflev=0.05,
                                     hr=FALSE) {

  if (is.list(strata) && length(strata) > 1) {
    if (!requireNamespace("foreach"))
      stop("package 'foreach' needed in descrSurvEstimate")

    if (!is.factor(strata[[1]]))
      strata <- as.factor(strata[[1]])

    if (missing(stratorder)) {
      useorder <- levels(droplevels(strata[[1]]))
    } else {
      useorder <- stratorder
    }

    strat <- '' ## hack to make R CMD check happy
    res <- foreach(strat=useorder, .combine=cbind) %do% {
      sidx <- idx & (strata[[1]] == strat)
      lres <- descrSurvEstimate.intern(S,
                                       strata[-1],
                                       sidx,
                                       survname,
                                       evaltimes,
                                       evaltimeunits,
                                       digits,
                                       includeNAs,
                                       missingName,
                                       pval,
                                       pvals_formatting,
                                       pvals_digits,
                                       pvals_signiflev,
                                       hr)
      if (stratheader)
        lres <- rbind(lres[1,],
                      c("\\vert",
                        ifelse(is.null(names(strata)[1]),
                               as.character(strat),
                               paste(names(strata)[1],
                                     as.character(strat), sep="=")),
                        rep("", ncol(lres) - ifelse(pval, 3, 2)),
                        ifelse(pval, "", NULL)),
                      lres[-1,])
      lres}
  } else {
    if (!is.list(strata))
      strata <- list(strata)

    if (!is.factor(strata[[1]]))
      strata <- as.factor(strata[[1]])

    if (missing(stratorder)) {
      useorder <- levels(droplevels(strata[[1]]))
    } else {
      useorder <- stratorder
    }

    res <- foreach(strat=useorder, .combine=cbind) %do% {
      sidx <- idx & (strata[[1]] == strat)
      lres <- descrSurvEstimate.single(S,
                                       sidx,
                                       survname,
                                       evaltimes,
                                       evaltimeunits,
                                       includeNAs,
                                       missingName,
                                       digits)
      ##colnames(lres)[2] <- ifelse(is.null(names(strata)[1]),
      ##                            as.character(strat),
      ##                            paste(names(strata)[1],
      ##                                  as.character(strat), sep="="))
      if (stratheader)
        lres <- rbind(lres[1,],
                      c("\\vert", ifelse(is.null(names(strata)[1]),
                                         as.character(strat),
                                         paste(names(strata)[1],
                                               as.character(strat), sep="="))),
                      lres[-1,])
      lres}
    if (pval) {
      coxres <- plyr::failwith(NA, coxCompSurv, quiet = TRUE)(S[idx,], strata[[1]][idx])
      if (pvals_formatting) {
        coxres$p.value <- prettyPvalues(coxres$p.value, digits=pvals_digits, signiflev = pvals_signiflev)
      }

      res <- cbind(res,
                   `p value`=c(
                       ifelse(hr,
                              paste0(coxres$p.value,
                                     " (",
                                     paste0(format(coxres$hr, digits=digits),
                                            " [",
                                            format(coxres$hr_l, digits=digits),
                                            "; ",
                                            format(coxres$hr_u, digits=digits),
                                            "]"),
                                     ")"),
                              coxres$p.value),
                       rep("", nrow(res)-1)))
    }
  }

  ##res[1,-1] <- colnames(res)[-1]
  ##colnames(res) <- NULL
  return(res)
}


descrSurvEstimate.single <- function(S,
                                     idx,
                                     survname="survival time",
                                     evaltimes=c(3,5),
                                     evaltimeunits="years",
                                     includeNAs=TRUE,
                                     missingName="missing",
                                     digits=2) {

  if (!requireNamespace("survival")) stop("Survival Estimate needs the 'survival' package")

  if (missing(idx))
    idx <- 1:nrow(S)

  Sfit <- survival::survfit(S[idx,] ~ 1)

  dtbl <- c(survname, "")
  for (year in evaltimes) {
    survprob <- pred.survfit(Sfit, year)
    newrow <- c(paste0("\\vert ", year, "-", evaltimeunits, " estimate"),
                ifelse(length(survprob)==0, "", format(survprob, digits=digits)))
    dtbl <- rbind(dtbl,
                  newrow)
  }
  if (includeNAs) {
    newrow <- c(paste("\\vert", missingName),
                sum(is.na(S[idx,])))
    dtbl <- rbind(dtbl,
                  newrow)
  }

  rownames(dtbl) <- NULL

  dtbl
}

coxCompSurv <- function(S, strat)
  {
    coxres <- plyr::failwith(NA, survival::coxph, quiet = TRUE)(S ~ strat)

    if (is.na(coxres))
        return(list(p.value=NA, hr=NA, hr_l=NA, hr_u=NA))

    sumcoxres <- summary(coxres)
    return(list(p.value=sumcoxres$logtest["pvalue"],
                hr=sumcoxres$conf.int[1],
                hr_l=sumcoxres$conf.int[3],
                hr_u=sumcoxres$conf.int[4]))
  }

splitGrps <- function(values, grouping) {
  glevs <- unique(grouping)

  res <-
    lapply(glevs, function(glev)
      values[grouping == glev])

  names(res) <- glevs

  res
}


##' Replace German Umlaute
##'
##' @param txt character.  within this txt the German umlauts will be replaced
##' @return character.  version of \code{txt} with all 'Umlaute' and
##'   'scharfes s' replaced.
##' @author Andreas Leha
##' @export
##' @examples
##' replaceGermanUmlauts("gefräßig")
replaceGermanUmlauts <- function(txt) {
  txt = gsub("\u00E4","ae",txt, fixed=TRUE)
  txt = gsub("\u00F6","oe",txt, fixed=TRUE)
  txt = gsub("\u00FC","ue",txt, fixed=TRUE)
  txt = gsub("\u00DF","ss",txt, fixed=TRUE)

  txt = gsub("\u00C4","Ae",txt, fixed=TRUE)
  txt = gsub("\u00D6","Oe",txt, fixed=TRUE)
  txt = gsub("\u00DC","Ue",txt, fixed=TRUE)

  return(txt)
}
