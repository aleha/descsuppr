## time-of-day class

##' Conversion to and from 'Time of Day'
##'
##' Does not safeguard against "26:69".
##' @param x vector to be converted
##' @param ... unused
##' @return x converted to/from 'tod'
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' times <- c("8:53", NA, "22:30")
##'
##' ## some conversions
##' as.tod(times)
##' as.numeric(as.tod(times))
##'
##' is.tod(times)
##' is.tod(as.tod(times))
as.tod <- function(x) UseMethod("as.tod")

##' @rdname as.tod
##' @export
as.tod.character <- function(x)
{
  regex.tod <- "[[:digit:]]?[[:digit:]]:[[:digit:]]{2}"
  x <- as.character(x)
  if (!all(grepl(glue::glue("^{regex.tod}$"), x[!is.na(x)]))) stop("needs to in format HH:MM or H:MM ('%H:%M')")
  class(x) <- "tod"
  return(x)
}
as.tod.default <- as.tod.character


##' @rdname as.tod
##' @export
as.tod.circular <- function(x)
{
  x <- normalize.tod(x)
  hours <- floor(x+0.000001)
  minutes <- round((x - hours) * 60)
  res <- paste0(hours, ":", sprintf("%02d", minutes))
  res[!is.na(hours) & !is.na(minutes)] <- as.tod(res[!is.na(hours) & !is.na(minutes)])
  res[ is.na(hours) |  is.na(minutes)] <- as.tod(NA_character_)
  class(res) <- "tod"
  return(res)
}

##' @rdname as.tod
##' @export
is.tod <- function(x) inherits(x, "tod")


##' @rdname as.tod
##' @export
as.double.tod <- function(x, ...)
{
  hours.tod(x) + minutes.tod(x)/60
}

## make an S3 generic for circular
##' @rdname as.tod
##' @export
circular <- function(x, ...) UseMethod("circular")
## take the usual definition of circular,
## and set it to be the default method
##' @rdname as.tod
##' @export
circular.default <- circular::circular
formals(circular.default) <- c(formals(circular.default), alist(... = ))

##' @rdname as.tod
##' @export
circular.tod <- function(x, ...)
{
  x %>% as.numeric %>% circular::circular(units = "hours", template = "clock24")
}



##' Basic Functions on time-of-day Vectors
##'
##' These shoud just do whatever the same function does for character
##' vectors
##' @param x vector of class \code{tod}
##' @param i integer of indices
##' @param value of type \code{tod}.  to be inserted
##' @param ... to match the generics' arguments
##' @return as expected
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' times <- c("8:53", NA, "22:30")
##' times <- as.tod(times)
##'
##' length(times)
##' times[1]
##' times[1] <- "07:00"
##' c(times, times)
length.tod <- function(x) NextMethod()

##' @rdname length.tod
##' @export
`[.tod` <- function(x, i, ...) as.tod(NextMethod())

##' @rdname length.tod
##' @export
`[<-.tod` <- function(x, i, ..., value) {
  value <- as.tod(value)
  NextMethod()
}

##' @rdname length.tod
##' @export
`[[.tod` <- function(x, i, ...) as.tod(NextMethod())

##' @rdname length.tod
##' @export
`[[<-.tod` <- function(x, i, ..., value) {
  value <- as.tod(value)
  NextMethod()
}

##' @rdname length.tod
##' @export
c.tod <- function(...) as.tod(NextMethod())

##' Extract the hours/minutes Component of time-of-day
##'
##' @param x vector of class tod
##' @return numeric vector of length \code{length(x)} with hours/minutes
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' times <- as.tod(c("8:53", NA, "22:30"))
##' hours.tod(times)
##' minutes.tod(times)
hours.tod <- function(x)
{
  x %>% strsplit(":") %>% sapply(function(xi) xi[1]) %>% as.numeric
}

##' @export
##' @rdname hours.tod
minutes.tod <- function(x)
{
  x %>% strsplit(":") %>% sapply(function(xi) xi[2]) %>% as.numeric
}

##' Descriptive Statistics for time-of-day Vectors
##'
##' Functions to calculate descriptve values for timo-of-day vectors.
##' The heavy lifting is done by the \code{circular} package.
##'
##' These functions are meant to provide the least surprising
##' descriptive values.
##' @param x vector of class \code{tod}
##' @param na.rm often passed through to correponding functions in the circular packages.  Otherwise logitcal.  Defaults to \code{FALSE}.
##' @param ... additional args passed on to base stats or circular
##'   stats functions
##' @return descriptive values as would be returned for non-circular numeric vectors
##' @author Dr. Andreas Leha
##' @export
##' @examples
##' times <- as.tod(c("21:53", NA, "22:30", "23:10", "23:58", "00:15", "01:01"))
##'
##' mean(times)
##' mean(times, na.rm = TRUE)
mean.tod <- function(x, ...)
{
  x %>% circular %>% mean(...) %>% as.tod
}

normalize.tod <- function(x)
{
  idx_nNA <- !is.na(x)
  while(any(x[idx_nNA] < 0)) {
    x[idx_nNA][x[idx_nNA] < 0] <- x[idx_nNA][x[idx_nNA] < 0] + 24
  }
  while(any(x[idx_nNA] > 24)) {
    x[idx_nNA][x[idx_nNA] > 24] <- x[idx_nNA][x[idx_nNA] > 24] - 24
  }
  return(x)
}

## make an S3 generic for sd
##' @rdname mean.tod
##' @export
sd <- function(x, ...) UseMethod("sd")
## take the usual definition of sd,
## and set it to be the default method


##' @rdname mean.tod
##' @export
sd.default <- stats::sd
formals(sd.default) <- c(formals(sd.default), alist(... = ))
## create a method for our class "foo"

##' @rdname mean.tod
##' @export
sd.tod <- function(x, na.rm = FALSE, ...)
{
  lmin <- min(x, na.rm = na.rm)
  res <- circular(x) - circular(lmin)
  res <- normalize.tod(res)
  res <- sd(res, na.rm = na.rm, ...)
  class(res) <- c("circular", "numeric")
  res <- as.tod(res)
  res
}

##' @rdname mean.tod
##' @export
median.tod <- function(x, na.rm = FALSE, ...)
{
  res <- median(circular(x), na.rm = na.rm, ...)
  class(res) <- c("circular", "numeric")
  as.tod(res)
}

##' @rdname mean.tod
##' @export
min.tod <- function(..., na.rm = FALSE)
{
  x <- Reduce(c, list(...))
  
  if (!na.rm && any(is.na(x))) {
    res <- NA_character_
    class(res) <- "tod"
    return(res)
  }
  
  ##mmm <- x %>% mean.tod %>% as.numeric
  ##mdiffs <- x %>% as.numeric %>% sapply(function(xi) xi - mmm)
  mmm <- circular(mean(x, na.rm = na.rm))
  mdiffs <- sapply(circular(x), function(xi) xi - mmm)
  idx_nNA <- !is.na(mdiffs)
  mdiffs[idx_nNA][mdiffs[idx_nNA] > 12] <- mdiffs[idx_nNA][mdiffs[idx_nNA] > 12] - 24
  idx <- which.min(mdiffs)
  res <- x[idx]
  class(res) <- "tod"
  return(res)
}


##' @rdname mean.tod
##' @export
max.tod <- function(..., na.rm = FALSE)
{
  x <- Reduce(c, list(...))
  
  if (!na.rm && any(is.na(x))) {
    res <- NA_character_
    class(res) <- "tod"
    return(res)
  }
  
  ##mmm <- x %>% mean.tod %>% as.numeric
  ##mdiffs <- x %>% as.numeric %>% sapply(function(xi) xi - mmm)
  mmm <- circular(mean(x, na.rm = na.rm))
  mdiffs <- sapply(circular(x), function(xi) xi - mmm)
  idx_nNA <- !is.na(mdiffs)
  mdiffs[idx_nNA][mdiffs[idx_nNA] > 12] <- mdiffs[idx_nNA][mdiffs[idx_nNA] > 12] - 24
  idx <- which.max(mdiffs)
  res <- x[idx]
  class(res) <- "tod"
  return(res)
}


##' @rdname mean.tod
##' @export
quantile.tod <- function(x, ...) {
  args <- list(...)
  if (!"prob" %in% names(args)) {
    prob <- seq(0, 1, 0.25)
  } else {
    prob <- args$prob
    args <- args[names(args) != "prob"]
  }
  
  res1 <- do.call(quantile, c(list(x = circular(x), prob = prob),   args))
  res2 <- do.call(quantile, c(list(x = circular(x), prob = 1-prob), args))
  res <- as.tod.circular(res2)
  names(res) <- names(res1)
  return(res)
}
